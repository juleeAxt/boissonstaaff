package com.theboissonstaff.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.theboissonstaff.utility.Constant;

import java.util.ArrayList;


public class MenuModel implements Parcelable {
    @SerializedName(Constant.ID)
    private String id;
    @SerializedName(Constant.NAME)
    private String name;
    @SerializedName(Constant.PARENT_ID)
    private String parentId;
    @SerializedName(Constant.ORGANISATION_ID)
    private String organisationId;
    @SerializedName(Constant.MENU_ITEMS)
    private ArrayList<MenuItemModel> arrayList;
    @SerializedName(Constant.IMAGE_URL)
    private String imageURL;

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(String organisationId) {
        this.organisationId = organisationId;
    }

    public ArrayList<MenuItemModel> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<MenuItemModel> arrayList) {
        this.arrayList = arrayList;
    }

    public MenuModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.parentId);
        dest.writeString(this.organisationId);
        dest.writeTypedList(this.arrayList);
        dest.writeString(this.imageURL);
    }

    protected MenuModel(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.parentId = in.readString();
        this.organisationId = in.readString();
        this.arrayList = in.createTypedArrayList(MenuItemModel.CREATOR);
        this.imageURL = in.readString();
    }

    public static final Creator<MenuModel> CREATOR = new Creator<MenuModel>() {
        @Override
        public MenuModel createFromParcel(Parcel source) {
            return new MenuModel(source);
        }

        @Override
        public MenuModel[] newArray(int size) {
            return new MenuModel[size];
        }
    };
}
