package com.theboissonstaff.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.theboissonstaff.utility.Constant;

import java.util.ArrayList;


public class OrderModel implements Parcelable {
    @SerializedName(Constant.ID)
    private String id;
    @SerializedName(Constant.CREATED_AT)
    private String createdAt;
    @SerializedName(Constant.IS_ACTIVE)
    private String isActive;
    @SerializedName(Constant.STATUS)
    private String status;
    @SerializedName(Constant.TABLE_ID)
    private String tableId;
    @SerializedName(Constant.ORGANISATION_ID)
    private String organisationId;
    @SerializedName(Constant.TABLE)
    private TableModel tableModel;
    @SerializedName(Constant.ORDER_NUMBER)
    private String orderNumber;
    @SerializedName(Constant.ORGANISATION)
    private OrganisationModel organisationModel;
    @SerializedName(Constant.ORDER_LINES)
    private ArrayList<OrderLineModel> lineModelArrayList;

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public OrganisationModel getOrganisationModel() {
        return organisationModel;
    }

    public void setOrganisationModel(OrganisationModel organisationModel) {
        this.organisationModel = organisationModel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(String organisationId) {
        this.organisationId = organisationId;
    }

    public TableModel getTableModel() {
        return tableModel;
    }

    public void setTableModel(TableModel tableModel) {
        this.tableModel = tableModel;
    }

    public ArrayList<OrderLineModel> getLineModelArrayList() {
        return lineModelArrayList;
    }

    public void setLineModelArrayList(ArrayList<OrderLineModel> lineModelArrayList) {
        this.lineModelArrayList = lineModelArrayList;
    }

    public OrderModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.isActive);
        dest.writeString(this.status);
        dest.writeString(this.tableId);
        dest.writeString(this.organisationId);
        dest.writeParcelable(this.tableModel, flags);
        dest.writeString(this.orderNumber);
        dest.writeParcelable(this.organisationModel, flags);
        dest.writeTypedList(this.lineModelArrayList);
    }

    protected OrderModel(Parcel in) {
        this.id = in.readString();
        this.createdAt = in.readString();
        this.isActive = in.readString();
        this.status = in.readString();
        this.tableId = in.readString();
        this.organisationId = in.readString();
        this.tableModel = in.readParcelable(TableModel.class.getClassLoader());
        this.orderNumber = in.readString();
        this.organisationModel = in.readParcelable(OrganisationModel.class.getClassLoader());
        this.lineModelArrayList = in.createTypedArrayList(OrderLineModel.CREATOR);
    }

    public static final Creator<OrderModel> CREATOR = new Creator<OrderModel>() {
        @Override
        public OrderModel createFromParcel(Parcel source) {
            return new OrderModel(source);
        }

        @Override
        public OrderModel[] newArray(int size) {
            return new OrderModel[size];
        }
    };
}
