package com.theboissonstaff.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CartsModel implements Parcelable {
    private String id,itemName, price, unitPrice, itemId, quantity,flavour,flavourId, image, maxQuantity,organisationId;

    public String getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(String organisationId) {
        this.organisationId = organisationId;
    }

    public String getFlavourId() {
        return flavourId;
    }

    public void setFlavourId(String flavourId) {
        this.flavourId = flavourId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(String maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public String getFlavour() {
        return flavour;
    }

    public void setFlavour(String flavour) {
        this.flavour = flavour;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public CartsModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.itemName);
        dest.writeString(this.price);
        dest.writeString(this.unitPrice);
        dest.writeString(this.itemId);
        dest.writeString(this.quantity);
        dest.writeString(this.flavour);
        dest.writeString(this.flavourId);
        dest.writeString(this.image);
        dest.writeString(this.maxQuantity);
        dest.writeString(this.organisationId);
    }

    protected CartsModel(Parcel in) {
        this.id = in.readString();
        this.itemName = in.readString();
        this.price = in.readString();
        this.unitPrice = in.readString();
        this.itemId = in.readString();
        this.quantity = in.readString();
        this.flavour = in.readString();
        this.flavourId = in.readString();
        this.image = in.readString();
        this.maxQuantity = in.readString();
        this.organisationId = in.readString();
    }

    public static final Creator<CartsModel> CREATOR = new Creator<CartsModel>() {
        @Override
        public CartsModel createFromParcel(Parcel source) {
            return new CartsModel(source);
        }

        @Override
        public CartsModel[] newArray(int size) {
            return new CartsModel[size];
        }
    };
}
