package com.theboissonstaff.model;

public class CategoryModel {
    public int productImage;
    public String productName;

    public CategoryModel(String productName, int productImage) {
        this.productImage = productImage;
        this.productName = productName;
    }

    public CategoryModel(String productName) {
        this.productName = productName;
    }

    public int getProductImage() {
        return productImage;
    }

    public void setProductImage(int productImage) {
        this.productImage = productImage;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
