package com.theboissonstaff.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.theboissonstaff.utility.Constant;

public class OrderLineModel implements Parcelable {
    @SerializedName(Constant.ID)
    private String id;
    @SerializedName(Constant.CREATED_AT)
    private String createdAt;
    @SerializedName(Constant.UPDATE_AT)
    private String updatedAt;
    @SerializedName(Constant.IS_ACTIVE)
    private String isActive;
    @SerializedName(Constant.STATUS)
    private String status;
    @SerializedName(Constant.MENU_ITEM_ID)
    private String menuItemId;
    @SerializedName(Constant.QUANTITY)
    private String quantity;
    @SerializedName(Constant.PRICE)
    private String price;
    @SerializedName(Constant.ORDER_ID)
    private String orderId;
    @SerializedName(Constant.FLAVOUR_ID)
    private String flavourId;

    public MenuItemModel getMenuItemModel() {
        return menuItemModel;
    }

    public void setMenuItemModel(MenuItemModel menuItemModel) {
        this.menuItemModel = menuItemModel;
    }

    @SerializedName(Constant.MENU_ITEM)
    private MenuItemModel menuItemModel;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(String menuItemId) {
        this.menuItemId = menuItemId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getFlavourId() {
        return flavourId;
    }

    public void setFlavourId(String flavourId) {
        this.flavourId = flavourId;
    }

    public OrderLineModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.isActive);
        dest.writeString(this.status);
        dest.writeString(this.menuItemId);
        dest.writeString(this.quantity);
        dest.writeString(this.price);
        dest.writeString(this.orderId);
        dest.writeString(this.flavourId);
        dest.writeParcelable(this.menuItemModel, flags);
    }

    protected OrderLineModel(Parcel in) {
        this.id = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.isActive = in.readString();
        this.status = in.readString();
        this.menuItemId = in.readString();
        this.quantity = in.readString();
        this.price = in.readString();
        this.orderId = in.readString();
        this.flavourId = in.readString();
        this.menuItemModel = in.readParcelable(MenuItemModel.class.getClassLoader());
    }

    public static final Creator<OrderLineModel> CREATOR = new Creator<OrderLineModel>() {
        @Override
        public OrderLineModel createFromParcel(Parcel source) {
            return new OrderLineModel(source);
        }

        @Override
        public OrderLineModel[] newArray(int size) {
            return new OrderLineModel[size];
        }
    };
}
