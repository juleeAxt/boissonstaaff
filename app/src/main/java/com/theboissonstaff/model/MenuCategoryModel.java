package com.theboissonstaff.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.theboissonstaff.utility.Constant;

public class MenuCategoryModel implements Parcelable {
    @SerializedName(Constant.ID)
    private String id;
    @SerializedName(Constant.NAME)
    private String name;
    @SerializedName(Constant.ORGANISATION_ID)
    private String organisationId;
    @SerializedName(Constant.IMAGE_URL)
    private String imageURL;

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(String organisationId) {
        this.organisationId = organisationId;
    }

    public MenuCategoryModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.organisationId);
        dest.writeString(this.imageURL);
    }

    protected MenuCategoryModel(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.organisationId = in.readString();
        this.imageURL = in.readString();
    }

    public static final Creator<MenuCategoryModel> CREATOR = new Creator<MenuCategoryModel>() {
        @Override
        public MenuCategoryModel createFromParcel(Parcel source) {
            return new MenuCategoryModel(source);
        }

        @Override
        public MenuCategoryModel[] newArray(int size) {
            return new MenuCategoryModel[size];
        }
    };
}