package com.theboissonstaff.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.theboissonstaff.utility.Constant;

public class MenuItemModel implements Parcelable {
    @SerializedName(Constant.ID)
    private String id;
    @SerializedName(Constant.NAME)
    private String name;
    @SerializedName(Constant.DESCRIPTION)
    private String description;
    @SerializedName(Constant.ITEM_TYPE)
    private String itemType;
    @SerializedName(Constant.ITEM_SUB_TYPE)
    private String itemSubType;
    @SerializedName(Constant.PRICE)
    private String price;
    @SerializedName(Constant.MENU_CATEGORY_ID)
    private String menuCategoryId;
    @SerializedName(Constant.ORGANISATION_ID)
    private String organisationId;
    @SerializedName(Constant.BASE_PRICE)
    private String basePrice;
    @SerializedName(Constant.UOM)
    private String uom;
    @SerializedName(Constant.MIN_PRICE)
    private String minPrice;
    @SerializedName(Constant.MAX_PRICE)
    private String maxPrice;
    @SerializedName(Constant.MENU_ITEM_IMAGE)
    private String menuItemImage;
    @SerializedName(Constant.QUANTITY)
    private String quantity;
    @SerializedName(Constant.IMAGE)
    private String image;
    @SerializedName(Constant.IMAGE_URL)
    private String imageURL;
    private boolean isSelectFlavour;

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isSelectFlavour() {
        return isSelectFlavour;
    }

    public void setSelectFlavour(boolean selectFlavour) {
        isSelectFlavour = selectFlavour;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    public String getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getMenuItemImage() {
        return menuItemImage;
    }

    public void setMenuItemImage(String menuItemImage) {
        this.menuItemImage = menuItemImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getItemSubType() {
        return itemSubType;
    }

    public void setItemSubType(String itemSubType) {
        this.itemSubType = itemSubType;
    }

    public String getMenuCategoryId() {
        return menuCategoryId;
    }

    public void setMenuCategoryId(String menuCategoryId) {
        this.menuCategoryId = menuCategoryId;
    }

    public String getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(String organisationId) {
        this.organisationId = organisationId;
    }

    public String getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public MenuItemModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.itemType);
        dest.writeString(this.itemSubType);
        dest.writeString(this.price);
        dest.writeString(this.menuCategoryId);
        dest.writeString(this.organisationId);
        dest.writeString(this.basePrice);
        dest.writeString(this.uom);
        dest.writeString(this.minPrice);
        dest.writeString(this.maxPrice);
        dest.writeString(this.menuItemImage);
        dest.writeString(this.quantity);
        dest.writeString(this.image);
        dest.writeString(this.imageURL);
        dest.writeByte(this.isSelectFlavour ? (byte) 1 : (byte) 0);
    }

    protected MenuItemModel(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.itemType = in.readString();
        this.itemSubType = in.readString();
        this.price = in.readString();
        this.menuCategoryId = in.readString();
        this.organisationId = in.readString();
        this.basePrice = in.readString();
        this.uom = in.readString();
        this.minPrice = in.readString();
        this.maxPrice = in.readString();
        this.menuItemImage = in.readString();
        this.quantity = in.readString();
        this.image = in.readString();
        this.imageURL = in.readString();
        this.isSelectFlavour = in.readByte() != 0;
    }

    public static final Creator<MenuItemModel> CREATOR = new Creator<MenuItemModel>() {
        @Override
        public MenuItemModel createFromParcel(Parcel source) {
            return new MenuItemModel(source);
        }

        @Override
        public MenuItemModel[] newArray(int size) {
            return new MenuItemModel[size];
        }
    };
}
