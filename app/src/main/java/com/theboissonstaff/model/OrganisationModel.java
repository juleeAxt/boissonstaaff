package com.theboissonstaff.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.theboissonstaff.utility.Constant;


public class OrganisationModel implements Parcelable {
    @SerializedName(Constant.ID)
    private String id;
    @SerializedName(Constant.CREATED_BY_ID)
    private String createdById;
    @SerializedName(Constant.Line1)
    private String line1;
    @SerializedName(Constant.Line2)
    private String line2;
    @SerializedName(Constant.CITY)
    private String city;
    @SerializedName(Constant.STATE)
    private String state;
    @SerializedName(Constant.COUNTRY)
    private String country;
    @SerializedName(Constant.ZIPCODE)
    private String zipCode;
    @SerializedName(Constant.NAME)
    private String name;
    @SerializedName(Constant.PHONE_NUMBER)
    private String phoneNumber;
    @SerializedName(Constant.ISACTIVE)
    private Boolean isActive;
    @SerializedName(Constant.OWNERID)
    private String ownerId;
    @SerializedName(Constant.DISTANCE)
    private int distance;
    @SerializedName(Constant.LATITUDE)
    private String latitude;
    @SerializedName(Constant.LONGITUDE)
    private String longitude;


    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedById() {
        return createdById;
    }

    public void setCreatedById(String createdById) {
        this.createdById = createdById;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public static Creator<OrganisationModel> getCREATOR() {
        return CREATOR;
    }

    public OrganisationModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.createdById);
        dest.writeString(this.line1);
        dest.writeString(this.line2);
        dest.writeString(this.city);
        dest.writeString(this.state);
        dest.writeString(this.country);
        dest.writeString(this.zipCode);
        dest.writeString(this.name);
        dest.writeString(this.phoneNumber);
        dest.writeValue(this.isActive);
        dest.writeString(this.ownerId);
        dest.writeInt(this.distance);
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
    }

    protected OrganisationModel(Parcel in) {
        this.id = in.readString();
        this.createdById = in.readString();
        this.line1 = in.readString();
        this.line2 = in.readString();
        this.city = in.readString();
        this.state = in.readString();
        this.country = in.readString();
        this.zipCode = in.readString();
        this.name = in.readString();
        this.phoneNumber = in.readString();
        this.isActive = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.ownerId = in.readString();
        this.distance = in.readInt();
        this.latitude = in.readString();
        this.longitude = in.readString();
    }

    public static final Creator<OrganisationModel> CREATOR = new Creator<OrganisationModel>() {
        @Override
        public OrganisationModel createFromParcel(Parcel source) {
            return new OrganisationModel(source);
        }

        @Override
        public OrganisationModel[] newArray(int size) {
            return new OrganisationModel[size];
        }
    };
}
