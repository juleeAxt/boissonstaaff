package com.theboissonstaff.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.theboissonstaff.utility.Constant;


public class UserModel implements Parcelable {
    @SerializedName(Constant.ID)
    private String id;
    @SerializedName(Constant.EMAIL)
    private String email;
    @SerializedName(Constant.USERNAME)
    private String username;
    @SerializedName(Constant.ORGANISATION_ID)
    private String organisationId;
    @SerializedName(Constant.WAITER_PROFILE)
    private ProfileModel profileModel;
    @SerializedName(Constant.ORGANISATION)
    private OrganisationModel organisationModel;

    public OrganisationModel getOrganisationModel() {
        return organisationModel;
    }



    public void setOrganisationModel(OrganisationModel organisationModel) {
        this.organisationModel = organisationModel;
    }

    public String getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(String organisationId) {
        this.organisationId = organisationId;
    }

    public ProfileModel getProfileModel() {
        return profileModel;
    }

    public void setProfileModel(ProfileModel profileModel) {
        this.profileModel = profileModel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }



    public UserModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.email);
        dest.writeString(this.username);
        dest.writeString(this.organisationId);
        dest.writeParcelable(this.profileModel, flags);
        dest.writeParcelable(this.organisationModel, flags);
    }

    protected UserModel(Parcel in) {
        this.id = in.readString();
        this.email = in.readString();
        this.username = in.readString();
        this.organisationId = in.readString();
        this.profileModel = in.readParcelable(ProfileModel.class.getClassLoader());
        this.organisationModel = in.readParcelable(OrganisationModel.class.getClassLoader());
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel source) {
            return new UserModel(source);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };
}
