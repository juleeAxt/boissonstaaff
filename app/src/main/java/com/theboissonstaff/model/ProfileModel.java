package com.theboissonstaff.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.theboissonstaff.utility.Constant;

public class ProfileModel implements Parcelable {
    @SerializedName(Constant.FIRST_NAME)
    private String firstName;
    @SerializedName(Constant.LAST_NAME)
    private String lastName;
    @SerializedName(Constant.LINE_ONE)
    private String line1;
    @SerializedName(Constant.LINE_TWO)
    private String line2;
    @SerializedName(Constant.AVATAR)
    private String avatar;
    @SerializedName(Constant.AVATAR_URL)
    private String avatarUrl;
    @SerializedName(Constant.CITY)
    private String city;
    @SerializedName(Constant.STATE)
    private String state;
    @SerializedName(Constant.COUNTRY)
    private String country;
    @SerializedName(Constant.ZIP_CODE)
    private String zipCode;
    @SerializedName(Constant.GENDER)
    private String gender;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }



    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }


    public ProfileModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.line1);
        dest.writeString(this.line2);
        dest.writeString(this.avatar);
        dest.writeString(this.avatarUrl);
        dest.writeString(this.city);
        dest.writeString(this.state);
        dest.writeString(this.country);
        dest.writeString(this.zipCode);
        dest.writeString(this.gender);
    }

    protected ProfileModel(Parcel in) {
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.line1 = in.readString();
        this.line2 = in.readString();
        this.avatar = in.readString();
        this.avatarUrl = in.readString();
        this.city = in.readString();
        this.state = in.readString();
        this.country = in.readString();
        this.zipCode = in.readString();
        this.gender = in.readString();
    }

    public static final Creator<ProfileModel> CREATOR = new Creator<ProfileModel>() {
        @Override
        public ProfileModel createFromParcel(Parcel source) {
            return new ProfileModel(source);
        }

        @Override
        public ProfileModel[] newArray(int size) {
            return new ProfileModel[size];
        }
    };
}
