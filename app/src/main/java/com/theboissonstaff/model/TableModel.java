package com.theboissonstaff.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.theboissonstaff.utility.Constant;

public class TableModel implements Parcelable {
    @SerializedName(Constant.ID)
    private String id;
    @SerializedName(Constant.NAME)
    private String name;
    @SerializedName(Constant.TABLE_NUMBER)
    private String tableNumber;
    @SerializedName(Constant.QR_CODE)
    private String qrCode;

    public String getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(String tableNumber) {
        this.tableNumber = tableNumber;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public TableModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.tableNumber);
        dest.writeString(this.qrCode);
    }

    protected TableModel(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.tableNumber = in.readString();
        this.qrCode = in.readString();
    }

    public static final Creator<TableModel> CREATOR = new Creator<TableModel>() {
        @Override
        public TableModel createFromParcel(Parcel source) {
            return new TableModel(source);
        }

        @Override
        public TableModel[] newArray(int size) {
            return new TableModel[size];
        }
    };
}
