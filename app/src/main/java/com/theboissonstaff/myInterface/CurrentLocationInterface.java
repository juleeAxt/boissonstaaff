package com.theboissonstaff.myInterface;

import android.location.Location;

public interface CurrentLocationInterface {
    void getLocation(Location location);
}
