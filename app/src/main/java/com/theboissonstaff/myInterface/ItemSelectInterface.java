package com.theboissonstaff.myInterface;

public interface ItemSelectInterface {
    void itemSelect(String id, String title);
}
