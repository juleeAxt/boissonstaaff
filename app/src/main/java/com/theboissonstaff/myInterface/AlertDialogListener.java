package com.theboissonstaff.myInterface;



public interface AlertDialogListener {
    /**
     * Method will be called when Confirm.
     */
    void onConfirm(String flag);

}
