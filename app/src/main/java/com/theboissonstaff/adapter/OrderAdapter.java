package com.theboissonstaff.adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.theboissonstaff.R;
import com.theboissonstaff.activity.OrderDetailActivity;
import com.theboissonstaff.model.OrderModel;
import com.theboissonstaff.utility.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    private Context context;
    private ArrayList<OrderModel> arrayList;


    public OrderAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<OrderModel> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_rv_order, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final OrderModel model = arrayList.get(i);
        viewHolder.tvOrderId.setText(model.getOrderNumber());
        viewHolder.tvTableNo.setText(model.getTableModel().getTableNumber());
        viewHolder.tvName.setText(model.getOrganisationModel().getName());
        viewHolder.tvStatus.setText(model.getStatus());
        switch (model.getStatus()) {
            case Constant.IN_PROGRESS:
                viewHolder.ivStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_in_progress));
                break;
            case Constant.PAUSED:
                viewHolder.ivStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_paused));
                break;
            case Constant.COMPLETED:
                viewHolder.ivStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_complete));
                break;
        }

        viewHolder.rlMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, OrderDetailActivity.class);
                intent.putExtra(Constant.ORGANISATION_ID, model.getOrganisationId());
                intent.putExtra(Constant.ID, model.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return null != arrayList ? arrayList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_order_id)
        TextView tvOrderId;
        @BindView(R.id.tv_table_no)
        TextView tvTableNo;
        @BindView(R.id.iv_status)
        ImageView ivStatus;
        @BindView(R.id.rl_main)
        RelativeLayout rlMain;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}