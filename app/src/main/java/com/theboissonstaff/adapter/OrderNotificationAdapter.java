package com.theboissonstaff.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.theboissonstaff.R;
import com.theboissonstaff.model.OrderModel;
import com.theboissonstaff.utility.DialogUtility;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OrderNotificationAdapter extends RecyclerView.Adapter<OrderNotificationAdapter.ViewHolder> {
    private Context context;
    private List<OrderModel> arrayList;
    private String imageUrl = "";
    String btnSelction;
    public OrderNotificationAdapter(Context context, List<OrderModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    /* public void setData(ArrayList<CategoryModel> arrayList, String imageUrl) {
         this.arrayList = arrayList;
         this.imageUrl = imageUrl;
     }
 */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_rv_notificationlist, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final OrderModel model = arrayList.get(i);

        viewHolder.tvOrderAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 btnSelction= viewHolder.tvOrderAccept.getText().toString();
                DialogUtility.selctionBtn(context, btnSelction, viewHolder.tvOrderAccept, viewHolder.tvOrderReject);
            }
        });
        viewHolder.tvOrderReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 btnSelction = viewHolder.tvOrderReject.getText().toString();
                DialogUtility.selctionBtn(context, btnSelction, viewHolder.tvOrderAccept, viewHolder.tvOrderReject);
                //alertDialog.dismiss();
            }
        });

    }

    @Override
    public int getItemCount() {
        return null != arrayList ? arrayList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_orderid)
        TextView tvOrderId;
        @BindView(R.id.btn_order_accept)
        Button tvOrderAccept;
        @BindView(R.id.btn_order_reject)
        Button tvOrderReject;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}