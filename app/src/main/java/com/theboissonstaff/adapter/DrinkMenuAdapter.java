package com.theboissonstaff.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.theboissonstaff.R;
import com.theboissonstaff.model.MenuItemModel;
import com.theboissonstaff.model.MenuModel;

import java.util.ArrayList;

public class DrinkMenuAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private ArrayList<MenuModel> menuModelArrayList;


    public DrinkMenuAdapter(Context context) {
        this.mContext = context;

    }

    public void setData(ArrayList<MenuModel> menuModelArrayList) {
        this.menuModelArrayList = menuModelArrayList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return menuModelArrayList.get(groupPosition).getArrayList().size();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        MenuItemModel generalItem = menuModelArrayList.get(groupPosition).getArrayList().get(childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_rv_drink, null);
        }
        TextView txtTitle = convertView.findViewById(R.id.tv_name);
        // TextView tvAmount = convertView.findViewById(R.id.tv_amount);
        ImageView ivItemImage = convertView.findViewById(R.id.iv_image);

        /*txtTitle.setText(generalItem.getShopName());
        tvDate.setText(generalItem.getTXNDATE());
        tvAmount.setText(generalItem.getPaymentGross());
        tvTransactionType.setText(String.format("%s %s", mContext.getString(R.string.transaction_type), generalItem.getPaymentMode()));
        tvReceiptNo.setText(String.format("%s %s", mContext.getString(R.string.order_no), generalItem.getTxnId()));
        Glide.with(mContext).load(generalItem.getPaymentGatwayImage()).apply(new RequestOptions().placeholder(R.drawable.img_place_holder).error(R.drawable.img_place_holder)).into(ivPayImage);
*/
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return menuModelArrayList.get(groupPosition).getArrayList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return menuModelArrayList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return null != menuModelArrayList ? menuModelArrayList.size() : 0;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        MenuModel parentItem = menuModelArrayList.get(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_menu_header, null);
        }
        // expand child view
        ExpandableListView mExpandableListView = (ExpandableListView) parent;
        mExpandableListView.expandGroup(groupPosition);

        TextView txtTitle = convertView.findViewById(R.id.tv_header_name);
        TextView tvCount = convertView.findViewById(R.id.tv_item_count);
        txtTitle.setText(parentItem.getName());
        tvCount.setText(parentItem.getArrayList().size() + " " + mContext.getString(R.string.item));

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}