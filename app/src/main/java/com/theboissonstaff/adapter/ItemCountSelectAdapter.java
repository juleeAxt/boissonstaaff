package com.theboissonstaff.adapter;


import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.theboissonstaff.R;
import com.theboissonstaff.myInterface.ItemSelectInterface;
import com.theboissonstaff.utility.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ItemCountSelectAdapter extends RecyclerView.Adapter<ItemCountSelectAdapter.ViewHolder> {
    private Context context;
    private ArrayList<String> arrayList;
    private int lastSelectedPosition = 0;
    private ItemSelectInterface itemSelectInterface;


    public ItemCountSelectAdapter(Context context, ItemSelectInterface itemSelectInterface) {
        this.context = context;
        this.itemSelectInterface = itemSelectInterface;
    }

    public void setData(ArrayList<String> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_rv_item_count, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final String model = arrayList.get(i);
        viewHolder.tvTitle.setText(model);

        if (lastSelectedPosition == i) {
            viewHolder.tvTitle.setSelected(true);
            viewHolder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.white));
        } else {
            viewHolder.tvTitle.setSelected(false);
            viewHolder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.text_deselect));
        }
        if (viewHolder.tvTitle.isSelected()) {
            itemSelectInterface.itemSelect(model, Constant.QUANTITY);
        }
    }

    @Override
    public int getItemCount() {
        return null != arrayList ? arrayList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title)
        TextView tvTitle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }
    }
}