package com.theboissonstaff.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.theboissonstaff.R;
import com.theboissonstaff.model.MenuModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MenuItemFoodHeaderAdapter extends RecyclerView.Adapter<MenuItemFoodHeaderAdapter.ViewHolder> {
    private Context context;
    private ArrayList<MenuModel> arrayList;


    public MenuItemFoodHeaderAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<MenuModel> arrayList) {
        this.arrayList = arrayList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_food_menu_header, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final MenuModel model = arrayList.get(i);
        viewHolder.tvHeaderName.setText(model.getName());
        viewHolder.tvItemCount.setText(model.getArrayList().size()+" "+context.getString(R.string.item_count));
        MenuItemFoodAdapter menuItemAdapter = new MenuItemFoodAdapter(context);
        viewHolder.rvMenu.setLayoutManager(new LinearLayoutManager(context));
        menuItemAdapter.setData(model.getArrayList());
        viewHolder.rvMenu.setAdapter(menuItemAdapter);

    }

    @Override
    public int getItemCount() {
        return null != arrayList ? arrayList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rv_menu)
        RecyclerView rvMenu;
        @BindView(R.id.tv_item_count)
        TextView tvItemCount;
        @BindView(R.id.tv_header_name)
        TextView tvHeaderName;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}