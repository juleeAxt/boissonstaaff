package com.theboissonstaff.adapter;

import android.content.Context;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.theboissonstaff.fragment.MenuItemFragment;
import com.theboissonstaff.model.MenuCategoryModel;


import java.util.ArrayList;

public class HomePagerAdapter extends FragmentStatePagerAdapter {
    private Context mContext;
    private ArrayList<MenuCategoryModel> arrayList;
    private ViewPager tabLayout;

    public HomePagerAdapter(Context mContext, FragmentManager manager, ArrayList<MenuCategoryModel> arrayList,ViewPager tabLayout) {
        super(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.mContext = mContext;
        this.arrayList = arrayList;
        this.tabLayout = tabLayout;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        MenuItemFragment fragment = new MenuItemFragment();
        fragment.getTablayout(tabLayout,arrayList);
        return fragment;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return arrayList.get(position).getName();
    }
}