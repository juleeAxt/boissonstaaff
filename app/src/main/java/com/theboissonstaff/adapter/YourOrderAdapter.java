package com.theboissonstaff.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.theboissonstaff.R;
import com.theboissonstaff.model.MenuItemModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class YourOrderAdapter extends RecyclerView.Adapter<YourOrderAdapter.ViewHolder> {
    private Context context;
    private ArrayList<MenuItemModel> arrayList;
    private int flag;

    public YourOrderAdapter(Context context,int flag) {
        this.context = context;
        this.flag = flag;

    }

    public void setData(ArrayList<MenuItemModel> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_rv_your_order, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final MenuItemModel model = arrayList.get(i);
        if (flag != 1){
            viewHolder.viewLine.setVisibility(View.GONE);
            viewHolder.ivDelete.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return null != arrayList ? arrayList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.view_line)
        View viewLine;
        @BindView(R.id.iv_delete)
        ImageView ivDelete;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}