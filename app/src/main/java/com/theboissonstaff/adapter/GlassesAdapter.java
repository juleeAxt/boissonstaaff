package com.theboissonstaff.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.theboissonstaff.R;

public class GlassesAdapter extends RecyclerView.Adapter<GlassesAdapter.ViewHolder> {
    private Context context;
    private int listCount;

    public GlassesAdapter(Context context, int listCount) {
        this.context = context;
        this.listCount = listCount;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_rv_glasses, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
    }

    @Override
    public int getItemCount() {
        return listCount;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

        }
    }
}