package com.theboissonstaff.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.theboissonstaff.R;
import com.theboissonstaff.model.OrderLineModel;
import com.theboissonstaff.myInterface.ItemSelectInterface;
import com.theboissonstaff.utility.BaseUtility;
import com.theboissonstaff.utility.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemAdapter.ViewHolder> {
    private Context context;
    private ArrayList<OrderLineModel> arrayList;
    private ItemSelectInterface itemSelectInterface;

    public OrderItemAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<OrderLineModel> arrayList, ItemSelectInterface itemSelectInterface) {
        this.arrayList = arrayList;
        this.itemSelectInterface = itemSelectInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_rv_your_order, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final OrderLineModel model = arrayList.get(i);
        viewHolder.tvItemName.setText(String.format("%s x %s", model.getQuantity(), model.getMenuItemModel().getName()));
        viewHolder.tvPrice.setText(String.valueOf(BaseUtility.totalPrice(model.getPrice(), model.getQuantity())));
        viewHolder.tvStatus.setText(model.getStatus());
        switch (model.getStatus()) {
            case Constant.CANCELED:
                viewHolder.tvStatus.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_cancel));
                viewHolder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.cancel_color));
                viewHolder.viewLine.setVisibility(View.GONE);
                viewHolder.ivDelete.setVisibility(View.GONE);
                break;
            case Constant.COMPLETED:
                viewHolder.tvStatus.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_order_complete));

                viewHolder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.complete));
                viewHolder.viewLine.setVisibility(View.GONE);
                viewHolder.ivDelete.setVisibility(View.GONE);
                break;

            case Constant.INITIATED:
                viewHolder.tvStatus.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_initiated));
                viewHolder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.initiated));
                viewHolder.viewLine.setVisibility(View.VISIBLE);
                viewHolder.ivDelete.setVisibility(View.VISIBLE);
                break;

            case Constant.IN_PROGRESS:
                viewHolder.tvStatus.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_on_going));
                viewHolder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.in_progress));
                viewHolder.viewLine.setVisibility(View.GONE);
                viewHolder.ivDelete.setVisibility(View.GONE);
                break;
        }
        viewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (model.getStatus().equals(Constant.INITIATED)) {
                    itemSelectInterface.itemSelect(model.getId(), "");

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return null != arrayList ? arrayList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.view_line)
        View viewLine;
        @BindView(R.id.iv_delete)
        ImageView ivDelete;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_item_name)
        TextView tvItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}