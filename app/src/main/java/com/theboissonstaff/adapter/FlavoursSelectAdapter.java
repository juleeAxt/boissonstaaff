package com.theboissonstaff.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.theboissonstaff.R;
import com.theboissonstaff.activity.AddToCartDialogActivity;
import com.theboissonstaff.model.MenuItemModel;
import com.theboissonstaff.myInterface.SelectFlavoursInterface;
import com.theboissonstaff.utility.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FlavoursSelectAdapter extends RecyclerView.Adapter<FlavoursSelectAdapter.ViewHolder> {
    private Context context;
    private ArrayList<MenuItemModel> arrayList;
    private int lastSelectedPosition = -1;
    private SelectFlavoursInterface itemSelectInterface;

    public FlavoursSelectAdapter(Context context, SelectFlavoursInterface itemSelectInterface) {
        this.context = context;
        this.itemSelectInterface = itemSelectInterface;
    }

    public void setData(ArrayList<MenuItemModel> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_rv_flavours, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final MenuItemModel model = arrayList.get(i);
        viewHolder.tvTitle.setText(model.getName());
        if (lastSelectedPosition == i) {
            model.setSelectFlavour(true);
            viewHolder.tvTitle.setSelected(true);
            viewHolder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.white));

        } else {
            model.setSelectFlavour(false);
            viewHolder.tvTitle.setSelected(false);
            viewHolder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.text_deselect));
        }
        if (viewHolder.tvTitle.isSelected()){
            itemSelectInterface.selectFlavour(model.getMaxPrice(), Constant.PRICE);
        }

    }

    @Override
    public int getItemCount() {
        return null != arrayList ? arrayList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title)
        TextView tvTitle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tvTitle.isSelected()) {
                        lastSelectedPosition = -1;
                        itemSelectInterface.selectFlavour("", "");
                    } else {
                        lastSelectedPosition = getAdapterPosition();
                    }
                    notifyDataSetChanged();
                }
            });
        }
    }
}