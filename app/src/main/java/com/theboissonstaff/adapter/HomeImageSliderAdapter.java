package com.theboissonstaff.adapter;


import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.theboissonstaff.R;
import com.theboissonstaff.model.MenuModel;
import com.theboissonstaff.myInterface.ItemSelectInterface;
import com.theboissonstaff.myInterface.RecyclerViewClickListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HomeImageSliderAdapter extends RecyclerView.Adapter<HomeImageSliderAdapter.ViewHolder> {
    private Context context;
    private ArrayList<MenuModel> arrayList;
    private static RecyclerViewClickListener itemListener;

    public HomeImageSliderAdapter(Context context, ArrayList<MenuModel> arrayList,RecyclerViewClickListener itemListener) {
        this.context = context;
        this.arrayList = arrayList;
        this.itemListener  = itemListener;
    }

    public void setData(ArrayList<MenuModel> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.pager_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final MenuModel model = arrayList.get(i);
        viewHolder.tv_brandy.setText(model.getName());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int devicewidth = displaymetrics.widthPixels / 4;
        viewHolder.llMain.getLayoutParams().width = devicewidth;
        Glide.with(context).load(model.getImageURL()).apply(new RequestOptions().placeholder(R.drawable.image)).into(viewHolder.img_brandy);
        /*viewHolder.img_brandy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemListener.recyclerViewListClicked(view,this.getA);
            }
        });
*/


    }

    @Override
    public int getItemCount() {
        return null != arrayList ? arrayList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.ll_main)
        LinearLayout llMain;
        @BindView(R.id.img_view)
        ImageView img_brandy;
        @BindView(R.id.txt_brandy)
        TextView tv_brandy;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, getAdapterPosition());
        }
    }
}