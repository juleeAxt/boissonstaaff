package com.theboissonstaff.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.theboissonstaff.R;
import com.theboissonstaff.model.TableModel;
import com.theboissonstaff.myInterface.ItemSelectInterface;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TableSelectAdapter extends RecyclerView.Adapter<TableSelectAdapter.ViewHolder> {
    private Context context;
    private ArrayList<TableModel> arrayList;
    private int lastSelectedPosition = -1;
    private ItemSelectInterface itemSelectInterface;

    public TableSelectAdapter(Context context, ItemSelectInterface itemSelectInterface) {
        this.context = context;
        this.itemSelectInterface = itemSelectInterface;
    }

    public void setData(ArrayList<TableModel> arrayList) {
        this.arrayList = arrayList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_rv_table_count, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final TableModel model = arrayList.get(i);
        viewHolder.tvTitle.setText(model.getTableNumber());
        if (lastSelectedPosition == i) {
            viewHolder.tvTitle.setSelected(true);
            viewHolder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.white));

        } else {
            viewHolder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.black));
            viewHolder.tvTitle.setSelected(false);
        }

        if (viewHolder.tvTitle.isSelected()) {
            itemSelectInterface.itemSelect(model.getId(), model.getTableNumber());
        }

    }

    @Override
    public int getItemCount() {
        return null != arrayList ? arrayList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title)
        TextView tvTitle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }
    }
}