package com.theboissonstaff.adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.theboissonstaff.R;
import com.theboissonstaff.activity.AddToCartDialogActivity;
import com.theboissonstaff.model.MenuItemModel;
import com.theboissonstaff.utility.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MenuItemFoodAdapter extends RecyclerView.Adapter<MenuItemFoodAdapter.ViewHolder> {
    private Context context;
    private ArrayList<MenuItemModel> arrayList;


    public MenuItemFoodAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<MenuItemModel> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_rv_food, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final MenuItemModel model = arrayList.get(i);
        viewHolder.tvName.setText(model.getName());
        viewHolder.tvLowPrice.setText(model.getMinPrice());
        viewHolder.tvUnitPrice.setText(model.getPrice());
        Glide.with(context).load(model.getImageURL()).apply(new RequestOptions().placeholder(R.drawable.image).error(R.drawable.image)).into(viewHolder.ivImage);

        viewHolder.rlMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,AddToCartDialogActivity.class);
                intent.putExtra(Constant.ITEM_DATA,model);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return null != arrayList ? arrayList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_price)
        TextView tvLowPrice;
        @BindView(R.id.tv_unit_price)
        TextView tvUnitPrice;
        @BindView(R.id.rl_main)
        RelativeLayout rlMain;
        @BindView(R.id.iv_image)
        ImageView ivImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}