package com.theboissonstaff.adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.theboissonstaff.R;
import com.theboissonstaff.fragment.OrderFragment;

public class OrdersPagerAdapter extends FragmentStatePagerAdapter {
    private Context mContext;
    private ViewPager tabLayout;
    public OrdersPagerAdapter(Context mContext, FragmentManager manager, ViewPager tabLayout) {
        super(manager,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.mContext = mContext;
        this.tabLayout = tabLayout;
    }

    @Override
    public Fragment getItem(int position) {
        OrderFragment fragment = new OrderFragment();
        fragment.orderView(tabLayout);
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return mContext.getString(R.string.on_going);

        }else {
            return mContext.getString(R.string.completed);
        }
    }
}