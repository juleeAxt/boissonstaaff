package com.theboissonstaff.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.theboissonstaff.R;
import com.theboissonstaff.model.OrderLineModel;
import com.theboissonstaff.myInterface.ItemSelectInterface;
import com.theboissonstaff.utility.BaseUtility;
import com.theboissonstaff.utility.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class NewOrderItemAdapter extends RecyclerView.Adapter<NewOrderItemAdapter.ViewHolder> {
    private Context context;
    private ArrayList<OrderLineModel> arrayList;
    private ItemSelectInterface itemSelectInterface;

    public NewOrderItemAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<OrderLineModel> arrayList) {
        this.arrayList = arrayList;
       // this.itemSelectInterface = itemSelectInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_order_descripation, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final OrderLineModel model = arrayList.get(i);
        viewHolder.tvItemName.setText(String.format("%s x %s", model.getQuantity(), model.getMenuItemModel().getName()));


    }

    @Override
    public int getItemCount() {
        return null != arrayList ? arrayList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_item_name)
        TextView tvItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}