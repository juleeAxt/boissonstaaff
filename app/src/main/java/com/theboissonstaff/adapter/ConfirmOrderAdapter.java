package com.theboissonstaff.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.theboissonstaff.R;
import com.theboissonstaff.activity.OrderDetailActivity;
import com.theboissonstaff.model.CartsModel;
import com.theboissonstaff.myInterface.ItemSelectInterface;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConfirmOrderAdapter extends RecyclerView.Adapter<ConfirmOrderAdapter.ViewHolder> {
    private Context context;
    private List<CartsModel> arrayList;
    private ItemSelectInterface itemSelectInterface;

    public ConfirmOrderAdapter(Context context, ItemSelectInterface itemSelectInterface) {
        this.context = context;
        this.itemSelectInterface = itemSelectInterface;
    }

    public void setData(ArrayList<CartsModel> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_rv_cart_listing, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final CartsModel model = arrayList.get(i);
        viewHolder.tvProductName.setText(model.getItemName());
        viewHolder.tvQuantity.setText(model.getQuantity());
        viewHolder.tvPrice.setText(model.getPrice());
        // set glasses adapter
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(RecyclerView.HORIZONTAL);
        viewHolder.rvGlasses.setLayoutManager(manager);
        GlassesAdapter adapter = new GlassesAdapter(context, Integer.parseInt(model.getQuantity()));
        viewHolder.rvGlasses.setAdapter(adapter);
        viewHolder.ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OrderDetailActivity.addOrderId = "";
                itemSelectInterface.itemSelect(model.getId(), String.valueOf(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return null != arrayList ? arrayList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_product_name)
        TextView tvProductName;
        @BindView(R.id.iv_cancel)
        ImageView ivCancel;
        @BindView(R.id.tv_quantity)
        TextView tvQuantity;
        @BindView(R.id.rv_glasses)
        RecyclerView rvGlasses;
        @BindView(R.id.tv_price)
        TextView tvPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}