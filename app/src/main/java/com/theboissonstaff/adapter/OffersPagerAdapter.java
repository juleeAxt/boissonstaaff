package com.theboissonstaff.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.theboissonstaff.R;

import java.util.ArrayList;

public class OffersPagerAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<String> arrayList;

    public OffersPagerAdapter(Context context, ArrayList<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;

    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_vp_offers, null);
        TextView tv_offer = view.findViewById(R.id.tv_offer);
        // Glide.with(context).load().apply(new RequestOptions().placeholder(R.drawable.img_place_holder).error(R.drawable.img_place_holder)).into(iv_image);
        container.addView(view);
        tv_offer.setText("Get Offer On");
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return null != arrayList ? arrayList.size() : 0;

    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }
}
