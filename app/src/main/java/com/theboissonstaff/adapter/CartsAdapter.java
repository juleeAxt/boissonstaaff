package com.theboissonstaff.adapter;


import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.theboissonstaff.R;
import com.theboissonstaff.model.CartsModel;
import com.theboissonstaff.myInterface.ItemSelectInterface;
import com.theboissonstaff.sqlite_helper.DatabaseHandler;
import com.theboissonstaff.utility.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CartsAdapter extends RecyclerView.Adapter<CartsAdapter.ViewHolder> {
    private Context context;
    private ArrayList<CartsModel> arrayList;
    private DatabaseHandler dbHandler;
    private ItemSelectInterface itemSelectInterface;


    public CartsAdapter(Context context,ItemSelectInterface itemSelectInterface) {
        this.context = context;
        this.itemSelectInterface = itemSelectInterface;
        dbHandler = new DatabaseHandler(context);
    }

    public void setData(ArrayList<CartsModel> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_carts, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final CartsModel model = arrayList.get(i);
        viewHolder.tvName.setText(model.getItemName());
        viewHolder.tvPrice.setText(model.getPrice());
        viewHolder.tvUnitPrice.setText(model.getUnitPrice());
        viewHolder.tvQuantity.setText(model.getQuantity());

        viewHolder.ivMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int quantityMinus = Integer.parseInt(viewHolder.tvQuantity.getText().toString());
                if (quantityMinus > 1) {
                    viewHolder.tvQuantity.setText(String.valueOf(quantityMinus - 1));
                    dbHandler.updateList(model.getId(), viewHolder.tvQuantity.getText().toString());
                    itemSelectInterface.itemSelect("", Constant.QUANTITY);
                }
            }
        });
        viewHolder.ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = Integer.parseInt(viewHolder.tvQuantity.getText().toString());
                int productQty = Integer.parseInt(model.getMaxQuantity());
                if (productQty > quantity) {
                    viewHolder.tvQuantity.setText(String.valueOf(quantity + 1));
                    dbHandler.updateList(model.getId(), viewHolder.tvQuantity.getText().toString());
                    itemSelectInterface.itemSelect("", Constant.QUANTITY);
                }else {
                    Toast.makeText(context, context.getString(R.string.quantity_select_error)+productQty, Toast.LENGTH_SHORT).show();
                }

            }
        });

        viewHolder.ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemSelectInterface.itemSelect(model.getId(),"");
            }
        });

    }

    @Override
    public int getItemCount() {
        return null != arrayList ? arrayList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.iv_cancel)
        ImageView ivCancel;
        @BindView(R.id.tv_unit_price)
        TextView tvUnitPrice;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.iv_minus)
        ImageView ivMinus;
        @BindView(R.id.iv_add)
        ImageView ivAdd;
        @BindView(R.id.tv_quantity)
        TextView tvQuantity;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}