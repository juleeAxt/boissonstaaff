package com.theboissonstaff.fragment;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.theboissonstaff.R;
import com.theboissonstaff.activity.DashboardActivity;
import com.theboissonstaff.activity.OrderDetailActivity;
import com.theboissonstaff.activity.TableSelectDialogActivity;
import com.theboissonstaff.adapter.CartsAdapter;
import com.theboissonstaff.model.CartsModel;
import com.theboissonstaff.myInterface.AlertDialogListener;
import com.theboissonstaff.myInterface.CurrentLocationInterface;
import com.theboissonstaff.myInterface.ItemSelectInterface;
import com.theboissonstaff.sqlite_helper.DatabaseHandler;
import com.theboissonstaff.utility.BaseUtility;
import com.theboissonstaff.utility.Constant;
import com.theboissonstaff.utility.DialogUtility;
import com.theboissonstaff.utility.LocationOnUtility;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CartFragment extends Fragment implements ItemSelectInterface, View.OnClickListener, AlertDialogListener, CurrentLocationInterface {
    @BindView(R.id.rv_item_list)
    RecyclerView rvItemList;
    @BindView(R.id.ll_no_data)
    LinearLayout llNoData;
    @BindView(R.id.ll_carts)
    LinearLayout llCarts;
    @BindView(R.id.tv_total_price)
    TextView tvTotalPrice;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.btn_add_item)
    Button btnAddItem;
    private Context mContext;
    private DatabaseHandler dbDatabaseHandler;
    private TextView tvCartsCount;
    private String itemId;
    private double latitude, longitude;

    public void getCartsCount(TextView tvCartsCount) {
        this.tvCartsCount = tvCartsCount;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        btnAddItem.setVisibility(View.VISIBLE);
        btnAddItem.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
        dbDatabaseHandler = new DatabaseHandler(mContext);
        if (!BaseUtility.isAppIsInBackground(mContext)) {
            new LocationOnUtility(mContext, getActivity(), this);
        }

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        setAdapter();
    }

    private void setAdapter() {
        ArrayList<CartsModel> cartsModelArrayList = dbDatabaseHandler.getData();
        if (cartsModelArrayList != null && !cartsModelArrayList.isEmpty()) {
            CartsAdapter adapter = new CartsAdapter(mContext, this);
            adapter.setData(cartsModelArrayList);
            rvItemList.setLayoutManager(new LinearLayoutManager(getContext()));
            rvItemList.setAdapter(adapter);
            llCarts.setVisibility(View.VISIBLE);
            llNoData.setVisibility(View.GONE);
            setTotalQtyCount(cartsModelArrayList);
        } else {
            llCarts.setVisibility(View.GONE);
            llNoData.setVisibility(View.VISIBLE);
        }
        BaseUtility.setCartsCount(tvCartsCount, mContext);

    }

    @Override
    public void itemSelect(String id, String title) {
        if (title.equals(Constant.QUANTITY)) {
            ArrayList<CartsModel> arrayList = dbDatabaseHandler.getData();
            setTotalQtyCount(arrayList);
        } else {
            itemId = id;
            DialogUtility.alertDialog(mContext, this, getString(R.string.delete_alert_msg), Constant.ADD_TO_CART_FLAG);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_confirm:
                if (BaseUtility.distanceInMeter(mContext, latitude, longitude)) {
                    OrderDetailActivity.addOrderId = "";
                    Intent intent = new Intent(mContext, TableSelectDialogActivity.class);
                    intent.putExtra(Constant.ITEM_DATA, dbDatabaseHandler.getData());
                    startActivity(intent);
                }
                break;

            case R.id.btn_add_item:
                BaseUtility.sentToScreenIntentWithFlag(mContext, DashboardActivity.class);
                break;
        }
    }

    private void setTotalQtyCount(ArrayList<CartsModel> list) {
        float totalQty = 0;
        for (CartsModel model : list) {
            totalQty = totalQty + BaseUtility.totalPrice(model.getPrice(), model.getQuantity());
        }
        tvTotalPrice.setText(String.format("%.2f", totalQty));
    }


    @Override
    public void onConfirm(String flag) {
        dbDatabaseHandler.deleteItem(itemId);
        setAdapter();
    }

    @Override
    public void getLocation(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }
}
