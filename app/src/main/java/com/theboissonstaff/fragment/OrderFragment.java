package com.theboissonstaff.fragment;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.theboissonstaff.R;
import com.theboissonstaff.adapter.OrderAdapter;
import com.theboissonstaff.model.OrderModel;
import com.theboissonstaff.server_networking.JsonArrayResponseListener;
import com.theboissonstaff.server_networking.NetworkHelper;
import com.theboissonstaff.server_networking.RequestHelper;
import com.theboissonstaff.utility.BaseUtility;
import com.theboissonstaff.utility.ConnectionUtil;
import com.theboissonstaff.utility.Constant;
import com.theboissonstaff.utility.ProgressDialogUtil;
import com.theboissonstaff.utility.Urls;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OrderFragment extends Fragment implements JsonArrayResponseListener {
    private static final String TAG = OrderFragment.class.getSimpleName();
    @BindView(R.id.rv_order)
    RecyclerView rvOrder;
    @BindView(R.id.ll_no_data)
    LinearLayout llNoData;
    @BindView(R.id.ll_order)
    LinearLayout llOrder;
    @BindView(R.id.tv_no_data)
    TextView tvNoData;
    private int flag;
    private Context mContext;
    private ProgressDialogUtil progressDialogUtil;
    private OrderAdapter orderAdapter;
    private ViewPager tabViewPager;

    public void orderView(ViewPager tabViewPager) {
        this.tabViewPager = tabViewPager;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        tvNoData.setText(getString(R.string.no_result_found));
        progressDialogUtil = new ProgressDialogUtil(mContext);
        setAdapter();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }


    private void orderListApi(String status) {
        if (ConnectionUtil.isInternetOn(mContext)) {
            RequestHelper.getRequest(NetworkHelper.REQ_CODE_ORDER_ON_GOING, mContext, Urls.ORDERS_URL + new NetworkHelper(mContext).orderOnGoingJson(status), this);
        } else {
            BaseUtility.toastMsg(getActivity(), getString(R.string.no_internet_connection));
        }
    }


    private void setAdapter() {
        orderAdapter = new OrderAdapter(mContext);
        rvOrder.setLayoutManager(new LinearLayoutManager(mContext));
        rvOrder.setAdapter(orderAdapter);
    }

    @Override
    public void onSuccess(int requestCode, JSONArray json) {
        switch (requestCode) {
            case NetworkHelper.REQ_CODE_ORDER_ON_GOING:
                progressDialogUtil.dismissDialog();
                Log.e(TAG, json.toString());
                if (json.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    Type typeList = new TypeToken<List<OrderModel>>() {
                    }.getType();
                    ArrayList<OrderModel> arrayList = gson.fromJson(String.valueOf(json), typeList);
                    if (arrayList != null && !arrayList.isEmpty()) {
                        orderAdapter.setData(arrayList);
                        orderAdapter.notifyDataSetChanged();
                    }
                } else {
                    llNoData.setVisibility(View.VISIBLE);
                    llOrder.setVisibility(View.GONE);
                }
                break;
        }
    }

    @Override
    public void onError() {
        progressDialogUtil.dismissDialog();
        llNoData.setVisibility(View.VISIBLE);
        llOrder.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        flag = tabViewPager.getCurrentItem();
        if (flag == 0) {
            orderListApi(Constant.IN_PROGRESS);
        } else {
            orderListApi(Constant.COMPLETED);
        }
        Log.e("select tab = ", tabViewPager.getCurrentItem() + "");
    }
}