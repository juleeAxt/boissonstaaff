package com.theboissonstaff.fragment;



import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.theboissonstaff.R;
import com.theboissonstaff.adapter.OfferAdapter;
import com.theboissonstaff.adapter.OffersPagerAdapter;
import com.theboissonstaff.model.MenuItemModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OfferFragment extends Fragment {
    @BindView(R.id.vp_offer_pager)
    ViewPager vpOfferPager;
    @BindView(R.id.rv_offer)
    RecyclerView rvOffer;
    private Context mContext;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_offer, container, false);
        ButterKnife.bind(this,view);
        init();
        return view;
    }

    private void init() {
        setOfferBanner();
        setOfferAdapter();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private void setOfferBanner(){
        ArrayList<String> list = new ArrayList<>();
        list.add("");
        list.add("");
        list.add("");
        OffersPagerAdapter adapter = new OffersPagerAdapter(mContext,list);
        vpOfferPager.setAdapter(adapter);
    }


    private void setOfferAdapter(){
        ArrayList<MenuItemModel> list = new ArrayList<>();
        list.add(new MenuItemModel());
        list.add(new MenuItemModel());
        list.add(new MenuItemModel());

        OfferAdapter adapter = new OfferAdapter(mContext);
        adapter.setData(list);
        rvOffer.setLayoutManager(new LinearLayoutManager(mContext));
        rvOffer.setAdapter(adapter);
    }

}
