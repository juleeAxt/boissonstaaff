package com.theboissonstaff.fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.theboissonstaff.R;
import com.theboissonstaff.adapter.HomeImageSliderAdapter;
import com.theboissonstaff.adapter.MenuItemFoodAdapter;
import com.theboissonstaff.adapter.MenuItemFoodHeaderAdapter;
import com.theboissonstaff.adapter.MenuItemHeaderAdapter;
import com.theboissonstaff.model.MenuCategoryModel;
import com.theboissonstaff.model.MenuItemModel;
import com.theboissonstaff.model.MenuModel;
import com.theboissonstaff.myInterface.ItemSelectInterface;
import com.theboissonstaff.myInterface.RecyclerViewClickListener;
import com.theboissonstaff.server_networking.JsonArrayResponseListener;
import com.theboissonstaff.server_networking.NetworkHelper;
import com.theboissonstaff.server_networking.RequestHelper;
import com.theboissonstaff.utility.BaseUtility;
import com.theboissonstaff.utility.ConnectionUtil;
import com.theboissonstaff.utility.Urls;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MenuItemFragment extends Fragment implements JsonArrayResponseListener, RecyclerViewClickListener {
    private static final String TAG = MenuItemFragment.class.getSimpleName();
    @BindView(R.id.rv_menu_drink)
    RecyclerView rvMenuDrink;
    @BindView(R.id.tv_auto_scroll)
    TextView tvAutoScroll;
    @BindView(R.id.rv_slider)
    RecyclerView rvSlider;
    @BindView(R.id.ll_top_slider)
    LinearLayout llTopSlider;
    @BindView(R.id.ll_no_data)
    LinearLayout llNoData;
    @BindView(R.id.tv_no_data)
    TextView tvNoData;
    private Context mContext;
    private Handler mHandler;
    private boolean wasInBackground = false;
    private MenuItemHeaderAdapter menuItemAdapter;
    private MenuItemFoodHeaderAdapter menuItemFoodAdapter;
    private int isScroll = 1;
    private ViewPager tabLayout;
    private ArrayList<MenuCategoryModel> arrayList;
    private LinearLayoutManager linearLayoutManager;

    public void getTablayout(ViewPager tabLayout, ArrayList<MenuCategoryModel> arrayList) {
        this.tabLayout = tabLayout;
        this.arrayList = arrayList;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_drink, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        tvNoData.setText(getText(R.string.order_not_found));
        showMarqueeText();
        setAdapter();

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    // this method call menu api
    private void menuListServerApi(String categoryId) {
        if (ConnectionUtil.isInternetOn(mContext)) {
            Log.e(TAG, "url == " + Urls.MENU_CATEGORIES + new NetworkHelper(mContext).menuItemJson(categoryId) + "");
            RequestHelper.getRequest(NetworkHelper.REQ_CODE_MENU, mContext, Urls.MENU_CATEGORIES + new NetworkHelper(mContext).menuItemJson(categoryId), this);
        } else {
            BaseUtility.toastMsg(mContext, getString(R.string.no_internet_connection));
        }
    }

    private void setHandler(int time) {
        final int FIVE_SECONDS = time;
        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            public void run() {
                if (wasInBackground) {
                    if (rvSlider.getVisibility() == View.VISIBLE && isScroll == 0) {
                        BaseUtility.slideToTop(llTopSlider);
                        rvSlider.setVisibility(View.GONE);
                        wasInBackground = false;
                    }
                }

            }
        }, FIVE_SECONDS);

    }

    private void setAdapter() {
        rvMenuDrink.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        isScroll = RecyclerView.SCROLL_STATE_IDLE;
                        setHandler(5000);
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        if (rvSlider.getVisibility() != View.VISIBLE) {
                            isScroll = RecyclerView.SCROLL_STATE_DRAGGING;
                            wasInBackground = true;
                            BaseUtility.slideToBottom(llTopSlider);
                            rvSlider.setVisibility(View.VISIBLE);
                        }
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        isScroll = RecyclerView.SCROLL_STATE_SETTLING;
                        break;

                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

    }

    private void setOfferAdapter(ArrayList menuItemModelArrayList) {
        if (menuItemModelArrayList != null && !menuItemModelArrayList.isEmpty()) {
            HomeImageSliderAdapter homeImageSliderAdapter = new HomeImageSliderAdapter(mContext, menuItemModelArrayList, this);
            LinearLayoutManager manager = new LinearLayoutManager(mContext);
            manager.setOrientation(RecyclerView.HORIZONTAL);
            rvSlider.setLayoutManager(manager);
            //homeImageSliderAdapter.setData(menuItemModelArrayList);
            rvSlider.setAdapter(homeImageSliderAdapter);


        }
    }

    private void showMarqueeText() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                //tvAutoScroll.setText(createMarqueeText());
                tvAutoScroll.requestFocus();
                tvAutoScroll.setSelected(true);
            }
        });
    }

    private String createMarqueeText() {
        StringBuilder text = new StringBuilder();
        text.append("Drinks &amp; Foods  $10.0");
        text.append("Drinks &amp; Foods  $ 10.0");
        text.append("Drinks &amp; Foods  $ 10.0");
        text.append("Drinks &amp; Foods  $ 10.0");
        return text.toString();
    }

    @Override
    public void onSuccess(int requestCode, JSONArray json) {
        switch (requestCode) {
            case NetworkHelper.REQ_CODE_MENU:
                if (json.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    Type listType = new TypeToken<List<MenuModel>>() {
                    }.getType();
                    ArrayList<MenuModel> menuItemModelArrayList = gson.fromJson(String.valueOf(json), listType);
                    //  menuItemAdapter.setData(menuItemModelArrayList);
                    // menuItemAdapter.notifyDataSetChanged();
                    if (tabLayout.getCurrentItem() == 0) {
                        menuItemAdapter = new MenuItemHeaderAdapter(mContext);
                        linearLayoutManager = new LinearLayoutManager(mContext);
                        rvMenuDrink.setLayoutManager(linearLayoutManager);
                        rvMenuDrink.setAdapter(menuItemAdapter);
                        menuItemAdapter.setData(menuItemModelArrayList);
                        menuItemAdapter.notifyDataSetChanged();
                    } else {
                        menuItemFoodAdapter = new MenuItemFoodHeaderAdapter(mContext);
                        linearLayoutManager = new LinearLayoutManager(mContext);
                        rvMenuDrink.setLayoutManager(linearLayoutManager);
                        rvMenuDrink.setAdapter(menuItemFoodAdapter);
                        menuItemFoodAdapter.setData(menuItemModelArrayList);
                        menuItemFoodAdapter.notifyDataSetChanged();

                    }
                    setOfferAdapter(menuItemModelArrayList);
                    llNoData.setVisibility(View.GONE);
                    rvMenuDrink.setVisibility(View.VISIBLE);
                    break;
                } else {
                    llNoData.setVisibility(View.VISIBLE);
                    rvMenuDrink.setVisibility(View.GONE);
                }
        }
    }

    @Override
    public void onError() {
        llNoData.setVisibility(View.VISIBLE);
        rvMenuDrink.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("select tab = ", tabLayout.getCurrentItem() + "");
        menuListServerApi(arrayList.get(tabLayout.getCurrentItem()).getId());

    }


    @Override
    public void recyclerViewListClicked(View v, int position) {
        //Toast.makeText(mContext, "" + position, Toast.LENGTH_SHORT).show();
        linearLayoutManager.scrollToPositionWithOffset(position, 0);
        rvMenuDrink.scrollToPosition(position);
        setHandler(1000);
        // ((LinearLayoutManager)rvMenuDrink.getLayoutManager()).scrollToPositionWithOffset(menuItemAdapter.getItemViewType(position),200);
    }
}