package com.theboissonstaff.activity;

import androidx.appcompat.app.AppCompatActivity;


import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.theboissonstaff.R;
import com.theboissonstaff.model.UserModel;
import com.theboissonstaff.server_networking.NetworkHelper;
import com.theboissonstaff.server_networking.RequestHelper;
import com.theboissonstaff.server_networking.ResponseListener;
import com.theboissonstaff.utility.BaseUtility;
import com.theboissonstaff.utility.ConnectionUtil;
import com.theboissonstaff.utility.Constant;
import com.theboissonstaff.utility.ProgressDialogUtil;
import com.theboissonstaff.utility.SessionManager;
import com.theboissonstaff.utility.Urls;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, ResponseListener {
    private static final String TAG = LoginActivity.class.getSimpleName();
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.ed_email)
    EditText edEmail;
    @BindView(R.id.ed_password)
    EditText edPassword;
    @BindView(R.id.cb_password_remember)
    CheckBox cbPasswordRemember;
    private ProgressDialogUtil progressDialogUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        progressDialogUtil = new ProgressDialogUtil(this);
        btnLogin.setOnClickListener(this);
        // set remember password
        if (!TextUtils.isEmpty(new SessionManager(this).getRememberPassword())) {
            edPassword.setText(new SessionManager(this).getRememberPassword());
            edEmail.setText(new SessionManager(this).getRememberUserName());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                attemptLogin();
                break;
        }
    }

    @Override
    public void onSuccess(int requestCode, JSONObject json) {
        switch (requestCode) {
            case NetworkHelper.REQ_CODE_LOGIN:
                Log.e(TAG, json.toString());
                String accessToken = json.optString(Constant.ACCESS_TOKEN);
                new SessionManager(this).setAccessToken("Bearer " + accessToken);
                if (cbPasswordRemember.isChecked()) {
                    new SessionManager(this).setRememberPassword(edPassword.getText().toString());
                    new SessionManager(this).setRememberUserName(edEmail.getText().toString());
                }
                loginVerifyApi();
                break;
            case NetworkHelper.REQ_CODE_VERIFY:
                progressDialogUtil.dismissDialog();
                Log.e(TAG, json.toString());
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                UserModel userModel = gson.fromJson(String.valueOf(json), UserModel.class);
                SessionManager sessionManager = new SessionManager(LoginActivity.this);
                sessionManager.saveUserDetail(userModel);
                BaseUtility.sentToScreenIntentWithFlag(this, DashboardActivity.class);
                break;
        }
    }

    @Override
    public void onError() {
        progressDialogUtil.dismissDialog();
    }

    private void attemptLogin() {
        edEmail.setError(null);
        edPassword.setError(null);
        String username = edEmail.getText().toString();
        String password = edPassword.getText().toString();
        if (TextUtils.isEmpty(username)) {
            edEmail.setError(getString(R.string.error_field_required));
        } else if (TextUtils.isEmpty(password)) {
            edPassword.setError(getString(R.string.error_field_required));
        } else {

            loginApi(username, password);
        }
    }

    // this method call login api
    private void loginApi(String userName, String password) {
        if (ConnectionUtil.isInternetOn(this)) {
            progressDialogUtil.showDialog();
            RequestHelper.PostRequest(NetworkHelper.REQ_CODE_LOGIN, this, Urls.LOGIN_URL, new NetworkHelper(this).loginJson(userName, password), this);
        } else {
            BaseUtility.toastMsg(this, getString(R.string.no_internet_connection));
        }
    }

    // this method call login verify api
    private void loginVerifyApi() {
        if (ConnectionUtil.isInternetOn(this)) {
            progressDialogUtil.showDialog();
            RequestHelper.verifyHeaderRequest(this, NetworkHelper.REQ_CODE_VERIFY, Urls.VERIFY_URL, this);
        } else {
            BaseUtility.toastMsg(this, getString(R.string.no_internet_connection));
        }
    }
}