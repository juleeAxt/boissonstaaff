package com.theboissonstaff.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.theboissonstaff.R;
import com.theboissonstaff.adapter.NewOrderItemAdapter;
import com.theboissonstaff.adapter.OrderItemAdapter;
import com.theboissonstaff.model.OrderModel;
import com.theboissonstaff.server_networking.JsonArrayResponseListener;
import com.theboissonstaff.server_networking.NetworkHelper;
import com.theboissonstaff.server_networking.RequestHelper;
import com.theboissonstaff.server_networking.ResponseListener;
import com.theboissonstaff.utility.BaseUtility;
import com.theboissonstaff.utility.ConnectionUtil;
import com.theboissonstaff.utility.Constant;
import com.theboissonstaff.utility.DialogUtility;
import com.theboissonstaff.utility.SessionManager;
import com.theboissonstaff.utility.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlertDialogNotificationActivity extends AppCompatActivity implements View.OnClickListener, JsonArrayResponseListener, ResponseListener {
    private static final String TAG = AlertDialogNotificationActivity.class.getSimpleName();
    @BindView(R.id.btn_order_accept)
    Button btnOrderAccept;
    @BindView(R.id.btn_order_reject)
    Button btnOrderReject;
    @BindView(R.id.tv_table_no)
    TextView tvTableNo;
    @BindView(R.id.tv_orderid)
    TextView tvOrderId;
    @BindView(R.id.rv_order)
    RecyclerView rvOrder;
    private NewOrderItemAdapter newOrderItemAdapter;
    private String btnSelction;
    private String orgId, orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_dialog_notification);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFinishOnTouchOutside(false);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        btnOrderAccept.setOnClickListener(this);
        btnOrderReject.setOnClickListener(this);
        if (getIntent().hasExtra(Constant.ORDER_ID)) {
            orderId = getIntent().getStringExtra(Constant.ORDER_ID);
            orgId = getIntent().getStringExtra(Constant.ORGANISATION_ID);

        }
        orderNotificationApi(orderId);
        setAdapter();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_order_accept:
                btnSelction = btnOrderAccept.getText().toString();
                DialogUtility.selctionBtn(AlertDialogNotificationActivity.this, btnSelction, btnOrderAccept, btnOrderReject);
                orderAcceptApi(orderId);
                break;
            case R.id.btn_order_reject:
                btnSelction = btnOrderReject.getText().toString();
                DialogUtility.selctionBtn(AlertDialogNotificationActivity.this, btnSelction, btnOrderAccept, btnOrderReject);
                finish();
                break;
        }
    }

    private void setAdapter() {
        newOrderItemAdapter = new NewOrderItemAdapter(this);
        rvOrder.setLayoutManager(new LinearLayoutManager(this));
        rvOrder.setAdapter(newOrderItemAdapter);
    }

    private void orderNotificationApi(String orderId) {
        if (ConnectionUtil.isInternetOn(this)) {
            RequestHelper.getRequest(NetworkHelper.REQ_CODE_ORDER_NOTIFICATION, this, Urls.ORDERS_URL + new NetworkHelper(this).getNewOrder(orderId), this);
        } else {
            BaseUtility.toastMsg(this, getString(R.string.no_internet_connection));
        }
    }

    private void orderAcceptApi(String orderId) {
        if (ConnectionUtil.isInternetOn(this)) {
            RequestHelper.PatchRequest(NetworkHelper.REQ_CODE_ORDER_ACCEPT, this, Urls.ORDERS_ACCEPT_URL + orderId, new NetworkHelper(this).getOrderAccept(), this);
        } else {
            BaseUtility.toastMsg(this, getString(R.string.no_internet_connection));
        }
    }

    @Override
    public void onSuccess(int requestCode, JSONArray json) {
        switch (requestCode) {
            case NetworkHelper.REQ_CODE_ORDER_NOTIFICATION:
                Log.e(TAG, json.toString());
                if (json.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    try {
                        OrderModel orderModel = gson.fromJson(String.valueOf(json.getJSONObject(0)), OrderModel.class);
                        if (orderModel != null) {
                            newOrderItemAdapter.setData(orderModel.getLineModelArrayList());
                            newOrderItemAdapter.notifyDataSetChanged();
                            tvOrderId.setText(orderModel.getOrderNumber());
                            tvTableNo.setText(orderModel.getTableModel().getTableNumber());
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                    }

                }
                break;
        }
    }

    @Override
    public void onSuccess(int requestCode, JSONObject json) {
        switch (requestCode) {
            case NetworkHelper.REQ_CODE_ORDER_ACCEPT:
                Log.e(TAG, json.toString());
               // finish();
                break;
        }
    }

    @Override
    public void onError() {

    }
}
