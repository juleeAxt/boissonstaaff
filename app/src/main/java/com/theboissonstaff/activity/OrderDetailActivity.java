package com.theboissonstaff.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.theboissonstaff.R;
import com.theboissonstaff.adapter.OrderItemAdapter;
import com.theboissonstaff.model.OrderLineModel;
import com.theboissonstaff.model.OrderModel;
import com.theboissonstaff.myInterface.AlertDialogListener;
import com.theboissonstaff.myInterface.ItemSelectInterface;
import com.theboissonstaff.server_networking.JsonArrayResponseListener;
import com.theboissonstaff.server_networking.NetworkHelper;
import com.theboissonstaff.server_networking.RequestHelper;
import com.theboissonstaff.utility.BaseUtility;
import com.theboissonstaff.utility.ConnectionUtil;
import com.theboissonstaff.utility.Constant;
import com.theboissonstaff.utility.DialogUtility;
import com.theboissonstaff.utility.Urls;

import org.json.JSONArray;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderDetailActivity extends AppCompatActivity implements View.OnClickListener, JsonArrayResponseListener, ItemSelectInterface, AlertDialogListener {
    private static final String TAG = OrderDetailActivity.class.getSimpleName();
    @BindView(R.id.tv_status)
    TextView tvStatus;
    @BindView(R.id.tv_total_price)
    TextView tvTotalPrice;
    @BindView(R.id.tv_order_id)
    TextView tvOrderId;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.rv_order)
    RecyclerView rvOrder;
    @BindView(R.id.btn_order)
    Button btnOrder;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    private String orgId, orderId;
    private OrderItemAdapter orderItemAdapter;
    public static String addOrderId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        btnOrder.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        setAdapter();
        if (getIntent().hasExtra(Constant.ORGANISATION_ID)) {
            orgId = getIntent().getStringExtra(Constant.ORGANISATION_ID);
            orderId = getIntent().getStringExtra(Constant.ID);
            orderDetailApi();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_order:
                if (btnOrder.getText().toString().equals(getString(R.string.add_new_order))) {
                    addOrderId = orderId;
                }
                BaseUtility.sentToScreenIntentWithFlag(this, DashboardActivity.class);
                break;
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }

    private void setAdapter() {
        orderItemAdapter = new OrderItemAdapter(this);
        rvOrder.setLayoutManager(new LinearLayoutManager(this));
        rvOrder.setAdapter(orderItemAdapter);
    }

    private void orderDetailApi() {
        if (ConnectionUtil.isInternetOn(this)) {
            RequestHelper.getRequest(NetworkHelper.REQ_CODE_ORDER_ON_GOING, this, Urls.ORDERS_URL + new NetworkHelper(this).orderDetailsJson(orgId, orderId), this);
        } else {
            BaseUtility.toastMsg(this, getString(R.string.no_internet_connection));
        }
    }


    @Override
    public void onSuccess(int requestCode, JSONArray json) {
        switch (requestCode) {
            case NetworkHelper.REQ_CODE_ORDER_ON_GOING:
                Log.e(TAG, json.toString());
                if (json.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    try {
                        OrderModel orderModel = gson.fromJson(String.valueOf(json.getJSONObject(0)), OrderModel.class);
                        if (orderModel != null) {
                            orderItemAdapter.setData(orderModel.getLineModelArrayList(), this);
                            orderItemAdapter.notifyDataSetChanged();
                            tvStatus.setText(orderModel.getStatus());
                            tvOrderId.setText(orderModel.getOrderNumber());
                            tvToolbarTitle.setText(orderModel.getOrganisationModel().getName());
                            tvDate.setText(BaseUtility.convertDateFormat(orderModel.getCreatedAt()));
                            switch (orderModel.getStatus()) {
                                case Constant.IN_PROGRESS:
                                    btnOrder.setText(getString(R.string.add_new_order));
                                    tvStatus.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_on_going));
                                    tvStatus.setTextColor(ContextCompat.getColor(this, R.color.in_progress));
                                    break;
                                case Constant.PAUSED:
                                    btnOrder.setText(getString(R.string.new_order));
                                    tvStatus.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_initiated));
                                    tvStatus.setTextColor(ContextCompat.getColor(this, R.color.initiated));
                                    break;
                                case Constant.COMPLETED:
                                    btnOrder.setText(getString(R.string.new_order));
                                    tvStatus.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_order_complete));
                                    tvStatus.setTextColor(ContextCompat.getColor(this, R.color.complete));
                                    break;
                            }
                            setTotalPrice(orderModel.getLineModelArrayList());
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                    }

                }
                break;

            case NetworkHelper.REQ_CODE_ORDER_DELETE:
                orderDetailApi();
                break;
        }
    }

    @Override
    public void onError() {
    }

    private void orderDeleteApi(String id) {
        if (ConnectionUtil.isInternetOn(this)) {
            RequestHelper.deleteRequest(NetworkHelper.REQ_CODE_ORDER_DELETE, this, Urls.ORDERS_DELETE_URL + id, this);
        } else {
            BaseUtility.toastMsg(this, getString(R.string.no_internet_connection));
        }
    }

    private void setTotalPrice(ArrayList<OrderLineModel> list) {
        float totalQty = 0;
        for (OrderLineModel model : list) {
            if (!model.getStatus().equals(Constant.CANCELED)) {
                totalQty = totalQty + BaseUtility.totalPrice(model.getPrice(), model.getQuantity());
            }
        }
        tvTotalPrice.setText(String.format("%.2f", totalQty));
    }

    @Override
    public void itemSelect(String id, String title) {
        DialogUtility.alertDialog(this, this, getString(R.string.delete_alert_msg), id);
    }

    @Override
    public void onConfirm(String flag) {
        orderDeleteApi(flag);
    }
}