package com.theboissonstaff.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.theboissonstaff.R;
import com.theboissonstaff.adapter.TableSelectAdapter;
import com.theboissonstaff.model.TableModel;
import com.theboissonstaff.myInterface.ItemSelectInterface;
import com.theboissonstaff.server_networking.JsonArrayResponseListener;
import com.theboissonstaff.server_networking.NetworkHelper;
import com.theboissonstaff.server_networking.RequestHelper;
import com.theboissonstaff.utility.BaseUtility;
import com.theboissonstaff.utility.ConnectionUtil;
import com.theboissonstaff.utility.Constant;
import com.theboissonstaff.utility.SessionManager;
import com.theboissonstaff.utility.Urls;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TableSelectDialogActivity extends AppCompatActivity implements ItemSelectInterface, View.OnClickListener, JsonArrayResponseListener {
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.rv_count)
    RecyclerView rvCount;
    @BindView(R.id.tv_no_data)
    TextView tvNoData;
    private String tableNo;
    private String tableId;
    private TableSelectAdapter tableSelectAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_select_dialog);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFinishOnTouchOutside(false);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        setAdapter();
        ivClose.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        tableListApi();
    }

    private void setAdapter() {
        tableSelectAdapter = new TableSelectAdapter(this, this);
        rvCount.setLayoutManager(new LinearLayoutManager(this));
        rvCount.setAdapter(tableSelectAdapter);
    }

    @Override
    public void itemSelect(String id, String title) {
        Log.e("count = ", id + "");
        tableNo = title;
        tableId = id;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_close:
                onBackPressed();
                break;
            case R.id.btn_submit:
                if (TextUtils.isEmpty(tableNo)) {
                    BaseUtility.toastMsg(this, getString(R.string.please_select_table));
                } else {
                    Intent intent = new Intent(this, ConfirmOrderActivity.class);
                    intent.putExtra(Constant.ITEM_DATA, getIntent().getParcelableArrayListExtra(Constant.ITEM_DATA));
                    intent.putExtra(Constant.TABLE_NUMBER, tableNo);
                    intent.putExtra(Constant.TABLE_ID, tableId);
                    startActivity(intent);
                    finish();
                }
                break;
        }
    }

    // this method call table api
    private void tableListApi() {
        if (ConnectionUtil.isInternetOn(this)) {
            RequestHelper.getRequest(NetworkHelper.REQ_CODE_TABLE, this, Urls.MENU_TABLE + new SessionManager(this).getOrganisationId() + "/available", this);
        } else {
            BaseUtility.toastMsg(this, getString(R.string.no_internet_connection));
        }
    }

    @Override
    public void onSuccess(int requestCode, JSONArray json) {
        switch (requestCode) {
            case NetworkHelper.REQ_CODE_TABLE:
                if (json.length() != 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    Type listType = new TypeToken<List<TableModel>>() {
                    }.getType();

                    ArrayList<TableModel> menuItemModelArrayList = gson.fromJson(String.valueOf(json), listType);
                    tableSelectAdapter.setData(menuItemModelArrayList);
                    tableSelectAdapter.notifyDataSetChanged();
                    rvCount.setVisibility(View.VISIBLE);
                    tvNoData.setVisibility(View.GONE);
                    btnSubmit.setVisibility(View.VISIBLE);
                } else {
                    rvCount.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                    btnSubmit.setVisibility(View.GONE);
                }
                break;
        }
    }

    @Override
    public void onError() {
        rvCount.setVisibility(View.GONE);
        tvNoData.setVisibility(View.VISIBLE);
        btnSubmit.setVisibility(View.GONE);
    }
}