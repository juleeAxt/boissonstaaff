package com.theboissonstaff.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.theboissonstaff.R;

import com.theboissonstaff.adapter.HomePagerAdapter;
import com.theboissonstaff.adapter.OrdersPagerAdapter;
import com.theboissonstaff.fragment.CartFragment;
import com.theboissonstaff.fragment.OfferFragment;
import com.theboissonstaff.model.MenuCategoryModel;
import com.theboissonstaff.server_networking.JsonArrayResponseListener;
import com.theboissonstaff.server_networking.NetworkHelper;
import com.theboissonstaff.server_networking.RequestHelper;
import com.theboissonstaff.utility.BaseUtility;
import com.theboissonstaff.utility.ConnectionUtil;
import com.theboissonstaff.utility.SessionManager;
import com.theboissonstaff.utility.Urls;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class DashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, JsonArrayResponseListener {
    private static final String TAG = DashboardActivity.class.getSimpleName();
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.tool_bar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.iv_menu)
    ImageView ivMenu;
    @BindView(R.id.vp_content)
    ViewPager vpContent;
    @BindView(R.id.vp_order_content)
    ViewPager vpOrderContent;
    @BindView(R.id.ll_home)
    LinearLayout llHome;
    @BindView(R.id.ll_carts)
    LinearLayout llCarts;
    @BindView(R.id.ll_offers)
    LinearLayout llOffers;
    @BindView(R.id.ll_order)
    LinearLayout llOrder;
    @BindView(R.id.view_home)
    View viewHome;
    @BindView(R.id.view_carts)
    View viewCarts;
    @BindView(R.id.view_offers)
    View viewOffers;
    @BindView(R.id.view_order)
    View viewOrder;
    @BindView(R.id.content_frame)
    FrameLayout contentFrame;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.tv_cart_count)
    TextView tvCartCount;
    private TextView tvUserName;
    private CircleImageView cmvProfile;
    private ArrayList<MenuCategoryModel> categoryModelArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashbord);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        setNavigationDrawer();
        BaseUtility.getCurrentTimeStamp();
        ivMenu.setOnClickListener(this);
        llHome.setOnClickListener(this);
        llCarts.setOnClickListener(this);
        llOrder.setOnClickListener(this);
        llOffers.setOnClickListener(this);
        ivNotification.setOnClickListener(this);
        getMenuCategory();
        // set profile image and name
        Log.e(TAG, "FCM = " + FirebaseInstanceId.getInstance().getToken());
        tvUserName.setText(new SessionManager(DashboardActivity.this).getFirstName());
        Glide.with(this).load(new SessionManager(this).getAvatarUrl()).apply(new RequestOptions().placeholder(R.drawable.image_profile).error(R.drawable.image_profile)).into(cmvProfile);
    }

    private void setNavigationDrawer() {
        // add navigation drawer
        View hView = navigationView.getHeaderView(0);
        tvUserName = hView.findViewById(R.id.tv_name);
        cmvProfile = hView.findViewById(R.id.cmv_profile);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(null);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        actionBarDrawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        // open home
        //sentToHomeFragment();
    }

    private void setHomeTabLayoutAdapter() {
        if (categoryModelArrayList != null && !categoryModelArrayList.isEmpty()) {
            HomePagerAdapter adapter = new HomePagerAdapter(this, getSupportFragmentManager(), categoryModelArrayList, vpContent);
            vpContent.setAdapter(adapter);
            vpContent.setOffscreenPageLimit(0);
            tabLayout.setupWithViewPager(vpContent);
            for (int i = 0; i < categoryModelArrayList.size(); i++) {
                tabLayout.getTabAt(i).setCustomView(R.layout.item_tablayout);
                View tab1_view = tabLayout.getTabAt(i).getCustomView();
                TextView tab1_title = (TextView) tab1_view.findViewById(R.id.txt_brandy);
                ImageView img1 = (ImageView) tab1_view.findViewById(R.id.img_view);
                tab1_title.setText(categoryModelArrayList.get(i).getName());
                Glide.with(DashboardActivity.this).load(categoryModelArrayList.get(i).getImageURL()).into(img1);
            }
        }
    }

    private void setOrderTabLayoutAdapter() {
        OrdersPagerAdapter adapter = new OrdersPagerAdapter(this, getSupportFragmentManager(), vpOrderContent);
        vpOrderContent.setAdapter(adapter);
        tabLayout.setupWithViewPager(vpOrderContent);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_on_going);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_completed);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if (item.isChecked())
                    item.setChecked(false);
                else
                    item.setChecked(true);

                //Closing drawer on item click
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        sentToHomeFragment();
                        break;
                    case R.id.nav_order:
                        sentToOrderFragment();
                        break;
                    case R.id.nav_about_us:
                        BaseUtility.sendActivityIntent(DashboardActivity.this, AboutUsActivity.class);
                        break;
                    case R.id.nav_term_and_condition:
                        BaseUtility.sendActivityIntent(DashboardActivity.this, TermAndConditionActivity.class);
                        break;
                    case R.id.nav_logout:
                        new SessionManager(DashboardActivity.this).logout();
                        break;
                }
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_menu:
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
                break;
            case R.id.ll_home:
                sentToHomeFragment();
                break;
            case R.id.ll_carts:
                sentToCartsFragment();
                break;
            case R.id.ll_offers:
                sentToOffersFragment();
                break;
            case R.id.iv_notification:
                BaseUtility.sendActivityIntent(this, NotificationActivity.class);
                break;
            case R.id.ll_order:
                sentToOrderFragment();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    // this method call home view
    private void sentToHomeFragment() {
        if (viewHome.getVisibility() != View.VISIBLE) {
            viewHome.setVisibility(View.VISIBLE);
            viewCarts.setVisibility(View.INVISIBLE);
            viewOffers.setVisibility(View.INVISIBLE);
            viewOrder.setVisibility(View.INVISIBLE);
            tabLayout.setVisibility(View.VISIBLE);
            vpContent.setVisibility(View.VISIBLE);
            contentFrame.setVisibility(View.GONE);
            vpOrderContent.setVisibility(View.GONE);
            tvToolbarTitle.setText("");

        }
        setHomeTabLayoutAdapter();
    }

    // this method call home order
    private void sentToOrderFragment() {
        if (vpOrderContent.getVisibility() != View.VISIBLE) {
            viewOrder.setVisibility(View.VISIBLE);
            vpOrderContent.setVisibility(View.VISIBLE);
            viewCarts.setVisibility(View.INVISIBLE);
            viewOffers.setVisibility(View.INVISIBLE);
            viewHome.setVisibility(View.INVISIBLE);
            tabLayout.setVisibility(View.VISIBLE);
            vpContent.setVisibility(View.GONE);
            contentFrame.setVisibility(View.GONE);
            tvToolbarTitle.setText(R.string.orders);
            setOrderTabLayoutAdapter();
        }
    }

    // this method call home fragment
    private void sentToCartsFragment() {
        if (viewCarts.getVisibility() != View.VISIBLE) {
            viewHome.setVisibility(View.INVISIBLE);
            viewCarts.setVisibility(View.VISIBLE);
            viewOffers.setVisibility(View.INVISIBLE);
            viewOrder.setVisibility(View.INVISIBLE);
            tabLayout.setVisibility(View.GONE);
            vpContent.setVisibility(View.GONE);
            vpOrderContent.setVisibility(View.GONE);
            contentFrame.setVisibility(View.VISIBLE);
            tvToolbarTitle.setText(getString(R.string.carts));
            FragmentTransaction manager = getSupportFragmentManager().beginTransaction();
            CartFragment fragment = new CartFragment();
            fragment.getCartsCount(tvCartCount);
            manager.replace(R.id.content_frame, fragment);
            manager.commit();
        }
    }

    // this method call home fragment
    private void sentToOffersFragment() {
        if (viewOffers.getVisibility() != View.VISIBLE) {
            viewHome.setVisibility(View.INVISIBLE);
            viewCarts.setVisibility(View.INVISIBLE);
            viewOffers.setVisibility(View.VISIBLE);
            viewOrder.setVisibility(View.INVISIBLE);
            tabLayout.setVisibility(View.GONE);
            vpContent.setVisibility(View.GONE);
            vpOrderContent.setVisibility(View.GONE);
            contentFrame.setVisibility(View.VISIBLE);
            tvToolbarTitle.setText(getString(R.string.offers));
            FragmentTransaction manager = getSupportFragmentManager().beginTransaction();
            manager.replace(R.id.content_frame, new OfferFragment());
            //manager.addToBackStack(null);
            manager.commit();
        }
    }

    // this method call menu category api
    private void getMenuCategory() {
        if (ConnectionUtil.isInternetOn(this)) {
            Log.e(TAG, "url == " + Urls.MENU_CATEGORIES + new NetworkHelper(this).menuCategory() + "");
            RequestHelper.getRequest(NetworkHelper.REQ_CODE_MENU_CATEGORY, this, Urls.MENU_CATEGORIES + new NetworkHelper(this).menuCategory() + Urls.SHORT, this);
        } else {
            BaseUtility.toastMsg(this, getString(R.string.no_internet_connection));
        }
    }


    @Override
    public void onSuccess(int requestCode, JSONArray json) {
        if (requestCode == NetworkHelper.REQ_CODE_MENU_CATEGORY) {
            categoryModelArrayList = new ArrayList<>();
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            Type listType = new TypeToken<List<MenuCategoryModel>>() {
            }.getType();
            categoryModelArrayList = gson.fromJson(String.valueOf(json), listType);
            sentToHomeFragment();
        }

    }

    @Override
    public void onError() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        BaseUtility.setCartsCount(tvCartCount, this);
    }


}