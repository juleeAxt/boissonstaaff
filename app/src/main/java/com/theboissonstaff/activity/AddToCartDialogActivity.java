package com.theboissonstaff.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.theboissonstaff.R;
import com.theboissonstaff.adapter.FlavoursSelectAdapter;
import com.theboissonstaff.adapter.ItemCountSelectAdapter;
import com.theboissonstaff.model.CartsModel;
import com.theboissonstaff.model.MenuItemModel;
import com.theboissonstaff.myInterface.CurrentLocationInterface;
import com.theboissonstaff.myInterface.ItemSelectInterface;
import com.theboissonstaff.myInterface.SelectFlavoursInterface;
import com.theboissonstaff.server_networking.NetworkHelper;
import com.theboissonstaff.server_networking.RequestHelper;
import com.theboissonstaff.server_networking.ResponseListener;
import com.theboissonstaff.sqlite_helper.DatabaseHandler;
import com.theboissonstaff.utility.BaseUtility;
import com.theboissonstaff.utility.ConnectionUtil;
import com.theboissonstaff.utility.Constant;
import com.theboissonstaff.utility.LocationOnUtility;
import com.theboissonstaff.utility.Urls;


import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddToCartDialogActivity extends AppCompatActivity implements View.OnClickListener, CurrentLocationInterface, ItemSelectInterface, ResponseListener, SelectFlavoursInterface {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.line_chart)
    LineChart lineChart;
    @BindView(R.id.iv_product_image)
    ImageView ivProductImage;
    @BindView(R.id.tv_item_name)
    TextView tvItemName;
    @BindView(R.id.tv_description)
    TextView tvDescription;
    @BindView(R.id.tv_price)
    TextView tvPrice;
    @BindView(R.id.rv_flavours)
    RecyclerView rvFlavours;
    @BindView(R.id.rv_item_count)
    RecyclerView rvItemCount;
    @BindView(R.id.iv_left_arrow)
    ImageView ivLeftArrow;
    @BindView(R.id.iv_right_arrow)
    ImageView ivRightArrow;
    @BindView(R.id.btn_add_to_carts)
    Button btnAddToCarts;
    @BindView(R.id.btn_proceed)
    Button btnProceed;
    @BindView(R.id.tv_flavours)
    TextView tvFlavours;
    private LinearLayoutManager manager;
    private MenuItemModel itemModel;
    private DatabaseHandler dbHandler;
    private String quantity = "1", flavourId = "", flavour, itemImage = "";
    private ArrayList<String> quantityList;
    private String menuItemId, totalQuantity;
    private FlavoursSelectAdapter flavoursSelectAdapter;
    private ArrayList<MenuItemModel> flavoursArrayList;
    private double latitude, longitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_cart_dialog);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFinishOnTouchOutside(false);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        if (!TextUtils.isEmpty(OrderDetailActivity.addOrderId)) {
            btnAddToCarts.setText(getString(R.string.cancel));
        } else {
            btnAddToCarts.setText(getString(R.string.add_to_cart));
        }
        if (!BaseUtility.isAppIsInBackground(this)) {
            new LocationOnUtility(this, this, this);
        }
        dbHandler = new DatabaseHandler(this);
        ivClose.setOnClickListener(this);
        ivLeftArrow.setOnClickListener(this);
        ivRightArrow.setOnClickListener(this);
        btnAddToCarts.setOnClickListener(this);
        btnProceed.setOnClickListener(this);
        getData();
        chart();
        setFlavoursAdapter();
        setItemQuantityAdapter();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_to_carts:
                if (btnAddToCarts.getText().toString().equals(getString(R.string.add_to_cart))) {
                    if (quantityValidation()) {
                        dbHandler.insert(itemModel, quantity, flavour, flavourId, totalQuantity, itemImage, tvPrice.getText().toString());
                        finish();
                    }
                } else {
                    OrderDetailActivity.addOrderId = "";
                    onBackPressed();
                }
                break;
            case R.id.btn_proceed:
                if (quantityValidation()) {
                    if (BaseUtility.distanceInMeter(this, latitude, longitude)) {
                        CartsModel model = new CartsModel();
                        model.setImage(itemImage);
                        model.setQuantity(quantity);
                        model.setFlavourId(flavourId);
                        model.setMaxQuantity(totalQuantity);
                        model.setFlavour(flavour);
                        model.setItemId(itemModel.getId());
                        model.setPrice(tvPrice.getText().toString());
                        model.setItemName(itemModel.getName());
                        model.setOrganisationId(itemModel.getOrganisationId());
                        model.setUnitPrice(tvPrice.getText().toString());
                        model.setId("0");
                        ArrayList<CartsModel> list = new ArrayList<>();
                        list.add(model);
                        Intent intent;
                        if (TextUtils.isEmpty(OrderDetailActivity.addOrderId)) {
                            intent = new Intent(this, TableSelectDialogActivity.class);
                        } else {
                            intent = new Intent(this, ConfirmOrderActivity.class);
                        }
                        intent.putExtra(Constant.ITEM_DATA, list);
                        startActivity(intent);
                        finish();
                    }
                }
                break;
            case R.id.iv_left_arrow:
                if (quantityList != null) {
                    if (manager.findFirstVisibleItemPosition() > 0) {
                        rvItemCount.smoothScrollToPosition(manager.findFirstVisibleItemPosition() - 1);
                    } else {
                        rvItemCount.smoothScrollToPosition(0);
                    }
                }
                break;
            case R.id.iv_right_arrow:
                if (quantityList != null) {
                    rvItemCount.smoothScrollToPosition(manager.findLastVisibleItemPosition() + 1);
                }
                break;
            case R.id.iv_close:
                OrderDetailActivity.addOrderId = "";
                onBackPressed();
                break;
        }
    }

    private boolean quantityValidation() {
        getSelectedFlavour();
        if (Integer.parseInt(quantity) > Integer.parseInt(totalQuantity)) {
            BaseUtility.toastMsg(this, getString(R.string.quantity_select_error) + totalQuantity);
            return false;
        } else {
            return true;
        }
    }

    private void getData() {
        if (getIntent().hasExtra(Constant.ITEM_DATA)) {
            itemModel = getIntent().getParcelableExtra(Constant.ITEM_DATA);
            if (itemModel != null) {
                totalQuantity = itemModel.getQuantity();
                menuItemId = itemModel.getId();
                tvTitle.setText(itemModel.getItemType());
                tvItemName.setText(itemModel.getName());
                tvDescription.setText(itemModel.getDescription());
                tvPrice.setText(itemModel.getPrice());
                itemImage = itemModel.getImageURL();
                Glide.with(this).load(itemModel.getImageURL()).apply(new RequestOptions().placeholder(R.drawable.image).error(R.drawable.image)).into(ivProductImage);
                flavoursApi();
            }
        }
    }

    private void setItemQuantityAdapter() {
        quantityList = new ArrayList<>();
        for (int i = 1; i <= 50; i++) {
            quantityList.add(String.valueOf(i));
        }
        manager = new LinearLayoutManager(this);
        manager.setOrientation(RecyclerView.HORIZONTAL);
        ItemCountSelectAdapter adapter = new ItemCountSelectAdapter(this, this);
        adapter.setData(quantityList);
        rvItemCount.setLayoutManager(manager);
        rvItemCount.setAdapter(adapter);
    }

    private void setFlavoursAdapter() {
        flavoursSelectAdapter = new FlavoursSelectAdapter(this, this);
        rvFlavours.setLayoutManager(new GridLayoutManager(this, 3));
        rvFlavours.setAdapter(flavoursSelectAdapter);
    }

    // this method call flavours api
    private void flavoursApi() {
        if (ConnectionUtil.isInternetOn(this)) {
            Log.e("url = ", Urls.MENU_ITEMS + menuItemId);
            RequestHelper.getRequestReturnObject(NetworkHelper.REQ_CODE_FLAVOURS, this, Urls.MENU_ITEMS + menuItemId, this);
        } else {
            BaseUtility.toastMsg(this, getString(R.string.no_internet_connection));
        }
    }

    private void chart() {
        ArrayList<Entry> entries = new ArrayList<>();
        entries.add(new Entry(0, 4));
        entries.add(new Entry(1, 1));
        entries.add(new Entry(2, 2));
        entries.add(new Entry(3, 4));
        LineDataSet dataSet = new LineDataSet(entries, null);
        dataSet.setColor(ContextCompat.getColor(this, R.color.colorPrimary));
        dataSet.setValueTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        //****
        // Controlling X axis
        XAxis xAxis = lineChart.getXAxis();
        // Set the xAxis position to bottom. Default is top
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        //Customizing x axis value
        final String[] months = new String[]{"Jan", "Feb", "Mar", "Apr"};

       /* IAxisValueFormatter formatter = new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return months[(int) value];
            }
        };
*/
        IndexAxisValueFormatter formatter = new IndexAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return months[(int) value];
            }
        };
        xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
        xAxis.setValueFormatter(formatter);

        //***
        // Controlling right side of y axis
        YAxis yAxisRight = lineChart.getAxisRight();
        yAxisRight.setEnabled(false);

        //***
        // Controlling left side of y axis
        YAxis yAxisLeft = lineChart.getAxisLeft();
        yAxisLeft.setGranularity(1f);

        // Setting Data
        LineData data = new LineData(dataSet);
        lineChart.setData(data);
        lineChart.animateX(2500);
        //refresh
        lineChart.invalidate();
    }

    @Override
    public void itemSelect(String id, String title) {
        if (title.equals(Constant.QUANTITY)) {
            quantity = id;
        }
    }

    @Override
    public void onSuccess(int requestCode, JSONObject json) {
        if (requestCode == NetworkHelper.REQ_CODE_FLAVOURS) {
            flavoursArrayList = new ArrayList<>();
            JSONArray array = json.optJSONArray(Constant.ITEM_DETAILS);
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            Type listType = new TypeToken<List<MenuItemModel>>() {
            }.getType();
            flavoursArrayList = gson.fromJson(String.valueOf(array), listType);
            if (flavoursArrayList != null && !flavoursArrayList.isEmpty()) {
                flavoursSelectAdapter.setData(flavoursArrayList);
                flavoursSelectAdapter.notifyDataSetChanged();
                tvFlavours.setVisibility(View.GONE);
                    /*tvPrice.setText(flavoursArrayList.get(0).getMaxPrice());
                    totalQuantity = flavoursArrayList.get(0).getQuantity();
                    flavour = flavoursArrayList.get(0).getName();
                    flavourId = flavoursArrayList.get(0).getId();*/
            } else {
                tvFlavours.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onError() {
    }

    private void getSelectedFlavour() {
        if (flavoursArrayList != null && !flavoursArrayList.isEmpty()) {
            for (MenuItemModel model : flavoursArrayList) {
                if (model.isSelectFlavour()) {
                    flavour = model.getName();
                    flavourId = model.getId();
                    tvPrice.setText(model.getMaxPrice());
                    totalQuantity = model.getQuantity();
                }
            }
        }
    }

    @Override
    public void selectFlavour(String price, String flag) {
        if (flag.equals(Constant.PRICE)) {
            tvPrice.setText(price);
        } else {
            tvPrice.setText(itemModel.getPrice());
            totalQuantity = itemModel.getPrice();
        }
    }

    @Override
    public void getLocation(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }
}