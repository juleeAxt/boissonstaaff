package com.theboissonstaff.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.theboissonstaff.R;
import com.theboissonstaff.adapter.ConfirmOrderAdapter;
import com.theboissonstaff.model.CartsModel;
import com.theboissonstaff.myInterface.AlertDialogListener;
import com.theboissonstaff.myInterface.ItemSelectInterface;
import com.theboissonstaff.server_networking.JsonArrayResponseListener;
import com.theboissonstaff.server_networking.NetworkHelper;
import com.theboissonstaff.server_networking.RequestHelper;
import com.theboissonstaff.server_networking.ResponseListener;
import com.theboissonstaff.sqlite_helper.DatabaseHandler;
import com.theboissonstaff.utility.BaseUtility;
import com.theboissonstaff.utility.ConnectionUtil;
import com.theboissonstaff.utility.Constant;
import com.theboissonstaff.utility.DialogUtility;
import com.theboissonstaff.utility.ProgressDialogUtil;
import com.theboissonstaff.utility.Urls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConfirmOrderActivity extends AppCompatActivity implements View.OnClickListener, ItemSelectInterface, ResponseListener, AlertDialogListener, JsonArrayResponseListener {
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.rv_item_list)
    RecyclerView rvConfirmList;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_total_amt)
    TextView tvTotalAmt;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.ll_no_data)
    LinearLayout llNoData;
    @BindView(R.id.ll_order)
    LinearLayout llOrder;
    @BindView(R.id.btn_add_item)
    Button btnAddItem;
    private ConfirmOrderAdapter confirmOrderAdapter;
    private ArrayList<CartsModel> orderList;
    private String tableNo, itemId, position, tableId;
    private DatabaseHandler databaseHandler;
    private ProgressDialogUtil progressDialogUtil;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        btnAddItem.setVisibility(View.VISIBLE);
        progressDialogUtil = new ProgressDialogUtil(this);
        databaseHandler = new DatabaseHandler(this);
        orderList = new ArrayList<>();
        ivBack.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
        btnAddItem.setOnClickListener(this);
        setAdapter();
        getOrderList();
    }

    // set menu adapter
    private void setAdapter() {
        confirmOrderAdapter = new ConfirmOrderAdapter(this, this);
        rvConfirmList.setLayoutManager(new LinearLayoutManager(this));
        rvConfirmList.setAdapter(confirmOrderAdapter);

    }

    private void getOrderList() {
        if (getIntent().hasExtra(Constant.ITEM_DATA)) {
            orderList = getIntent().getParcelableArrayListExtra(Constant.ITEM_DATA);
            if (getIntent().hasExtra(Constant.TABLE_NUMBER)) {
                tableNo = getIntent().getStringExtra(Constant.TABLE_NUMBER);
                tableId = getIntent().getStringExtra(Constant.TABLE_ID);
                tvToolbarTitle.setText(getString(R.string.table_no) + tableNo);
            }

            confirmOrderAdapter.setData(orderList);
            confirmOrderAdapter.notifyDataSetChanged();
            checkOrderNotEmpty();
            setTotalQtyCount();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.btn_confirm:
                if (TextUtils.isEmpty(OrderDetailActivity.addOrderId)) {
                    placeOrderApi();
                } else {
                    placeBulkOrderApi();
                }
                break;
            case R.id.btn_add_item:
                BaseUtility.sentToScreenIntentWithFlag(this, DashboardActivity.class);
                break;
        }
    }

    @Override
    public void itemSelect(String id, String title) {
        itemId = id;
        position = title;
        DialogUtility.alertDialog(this, this, getString(R.string.delete_alert_msg), Constant.ADD_TO_CART_FLAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        OrderDetailActivity.addOrderId = "";
     /*   if (orderList != null && !orderList.isEmpty()) {
            DialogUtility.alertDialog(this, this, getString(R.string.order_cancel_msg), "");
        } else {
            super.onBackPressed();
        }*/
    }

    @Override
    public void onSuccess(int requestCode, JSONObject json) {
        switch (requestCode) {
            case NetworkHelper.REQ_CODE_PLACE_ORDER:
                progressDialogUtil.dismissDialog();
                databaseHandler.deleteAllData();
                llNoData.setVisibility(View.VISIBLE);
                llOrder.setVisibility(View.GONE);
                orderList.clear();
                OrderDetailActivity.addOrderId = "";
                BaseUtility.toastMsg(this, getString(R.string.order_placed_msg));
                break;
        }
    }

    @Override
    public void onSuccess(int requestCode, JSONArray json) {
        switch (requestCode) {
            case NetworkHelper.REQ_CODE_PLACE_ORDER:
                progressDialogUtil.dismissDialog();
                databaseHandler.deleteAllData();
                llNoData.setVisibility(View.VISIBLE);
                llOrder.setVisibility(View.GONE);
                orderList.clear();
                OrderDetailActivity.addOrderId = "";
                BaseUtility.toastMsg(this, getString(R.string.order_placed_msg));
                break;
        }
    }

    @Override
    public void onError() {
        progressDialogUtil.dismissDialog();
    }

    private void checkOrderNotEmpty() {
        if (orderList != null && !orderList.isEmpty()) {
            llNoData.setVisibility(View.GONE);
            llOrder.setVisibility(View.VISIBLE);
        } else {
            llNoData.setVisibility(View.VISIBLE);
            llOrder.setVisibility(View.GONE);
        }
    }

    private void setTotalQtyCount() {
        float totalQty = 0;
        for (CartsModel model : orderList) {
            totalQty = totalQty + BaseUtility.totalPrice(model.getPrice(), model.getQuantity());
        }
        tvTotalAmt.setText(String.format("%.2f", totalQty));
    }

    // this method place order
    private void placeOrderApi() {
        if (ConnectionUtil.isInternetOn(this)) {
            progressDialogUtil.showDialog();
            RequestHelper.PostRequestWithoutHeader(NetworkHelper.REQ_CODE_PLACE_ORDER, this, Urls.ORDER, new NetworkHelper(this).placeOrder(orderList, tableId), this);
        } else {
            BaseUtility.toastMsg(this, getString(R.string.no_internet_connection));
        }
    } // this method place bulk order

    private void placeBulkOrderApi() {
        if (ConnectionUtil.isInternetOn(this)) {
            progressDialogUtil.showDialog();
            RequestHelper.postRequest(NetworkHelper.REQ_CODE_PLACE_ORDER, this, Urls.ORDER_BULk, new NetworkHelper(this).placeBulkOrder(orderList), this);
        } else {
            BaseUtility.toastMsg(this, getString(R.string.no_internet_connection));
        }
    }


    @Override
    public void onConfirm(String flag) {
        if (flag.equals(Constant.ADD_TO_CART_FLAG)) {
            orderList.remove(Integer.parseInt(position));
            confirmOrderAdapter.notifyDataSetChanged();
            if (!itemId.equals("0")) {
                databaseHandler.deleteItem(itemId);
            }
            checkOrderNotEmpty();
            setTotalQtyCount();
        } else {
            finish();
        }
    }
}