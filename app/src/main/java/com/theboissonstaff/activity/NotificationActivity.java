package com.theboissonstaff.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.theboissonstaff.R;
import com.theboissonstaff.adapter.OrderNotificationAdapter;
import com.theboissonstaff.model.OrderModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.rv_notification)
    RecyclerView rvNotification;
    private List<OrderModel> orderList = new ArrayList<>();
    private OrderNotificationAdapter orderNotificationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        tvToolbarTitle.setText(R.string.notification);
        ivBack.setOnClickListener(this);
        setOrderAdapter();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
        }

    }

    // set Hotel list adapter
    private void setOrderAdapter() {
        orderNotificationAdapter = new OrderNotificationAdapter(this, orderList);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(RecyclerView.VERTICAL);
        rvNotification.setLayoutManager(manager);
        rvNotification.setAdapter(orderNotificationAdapter);
        setOrderList();
    }

    private void setOrderList() {
        orderList.add(new OrderModel());
        orderList.add(new OrderModel());
        orderList.add(new OrderModel());
        orderList.add(new OrderModel());
        orderNotificationAdapter.notifyDataSetChanged();
    }

}
