package com.theboissonstaff.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


import com.theboissonstaff.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TermAndConditionActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.tv_toolbar_title)
    TextView tvtoolbarTitle;
    @BindView(R.id.iv_back)
    ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_and_condition);
        ButterKnife.bind(this);
        init();
    }


    private void init() {
        tvtoolbarTitle.setText(R.string.termandcondition);
        ivBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }
}
