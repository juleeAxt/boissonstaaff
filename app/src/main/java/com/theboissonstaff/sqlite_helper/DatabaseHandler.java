package com.theboissonstaff.sqlite_helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.theboissonstaff.model.CartsModel;
import com.theboissonstaff.model.MenuItemModel;
import com.theboissonstaff.utility.BaseUtility;


import java.util.ArrayList;


public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "Boisson";
    private static final String TABLE_ADD_TO_CART = "AddToCarts";
    private static final String ID = "id";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table AddToCarts " +
                        "(id text ,name text,count text,price text,unit_price text,itemId text,flavourId text, flavour text, quantityMax text, image text, organisationId text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADD_TO_CART);
        // Create tables again
        onCreate(db);
    }

    public void insert(MenuItemModel model, String count, String flavour, String flavourId, String maxQuantity, String image, String price) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", BaseUtility.getCurrentTimeStamp());
        contentValues.put("name", model.getName());
        contentValues.put("organisationId", model.getOrganisationId());
        contentValues.put("count", count);
        contentValues.put("price", price);
        contentValues.put("unit_price", price);
        contentValues.put("itemId", model.getId());
        contentValues.put("flavourId", flavourId);
        contentValues.put("flavour", flavour);
        contentValues.put("quantityMax", maxQuantity);
        contentValues.put("image", image);
        db.insert(TABLE_ADD_TO_CART, null, contentValues);
    }

    public ArrayList<CartsModel> getData() {
        ArrayList<CartsModel> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String select = "SELECT * FROM " + TABLE_ADD_TO_CART;
        Cursor cursor = db.rawQuery(select, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                CartsModel model = new CartsModel();
                model.setId(cursor.getString(0));
                model.setItemName(cursor.getString(1));
                model.setQuantity(cursor.getString(2));
                model.setPrice(cursor.getString(3));
                model.setUnitPrice(cursor.getString(4));
                model.setItemId(cursor.getString(5));
                model.setFlavourId(cursor.getString(6));
                model.setFlavour(cursor.getString(7));
                model.setMaxQuantity(cursor.getString(8));
                model.setImage(cursor.getString(9));
                model.setOrganisationId(cursor.getString(10));

                // Adding cart to list
                list.add(model);
            } while (cursor.moveToNext());
        }
        //closing the cursor
        cursor.close();
        // return add to cart list
        return list;
    }

    public void deleteItem(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ADD_TO_CART, ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    public void updateList(String id, String count) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("count", count);
        db.update(TABLE_ADD_TO_CART,
                contentValues,
                ID + " = ?",
                new String[]{id});

    }

    public void deleteAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM AddToCarts"); //delete all rows in a table
        db.close();
    }
}