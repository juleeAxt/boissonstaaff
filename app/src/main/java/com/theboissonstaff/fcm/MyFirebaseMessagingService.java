package com.theboissonstaff.fcm;

import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.theboissonstaff.R;
import com.theboissonstaff.activity.AlertDialogNotificationActivity;
import com.theboissonstaff.activity.NotificationActivity;
import com.theboissonstaff.utility.Constant;
import com.theboissonstaff.utility.DialogUtility;
import com.theboissonstaff.utility.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Objects;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "Notification";
    private NotificationUtils notificationUtils;
    private int notificationIndex = 0;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        super.onMessageReceived(remoteMessage);
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        Log.e("", "From: " + remoteMessage.getFrom());
        if (remoteMessage == null)
            return;
        if (new SessionManager(getApplicationContext()).isLogin()) {
            Intent resultIntent = null;
            // Check if message contains a notification payload.
            // new SessionManager(getApplicationContext()).isNotification(true);
            if (remoteMessage.getNotification() != null) {
                Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
                Map<String, String> params = remoteMessage.getData();
                JSONObject object = new JSONObject(params);
                try {
                    String title = object.getString(Constant.TITLE);
                    String message = object.getString(Constant.BODY);
                    String orderId = object.getString(Constant.ORDER_ID);
                    String organisationId = object.getString(Constant.ORGANISATION_ID);
                    Log.e(TAG, "title: " + title);
                    if (title.equals("New Order Notification")) {
                        resultIntent = new Intent(getApplicationContext(), AlertDialogNotificationActivity.class);
                        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                       // resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        resultIntent.putExtra(Constant.ORDER_ID, orderId);
                        resultIntent.putExtra(Constant.ORGANISATION_ID, organisationId);
                        startActivity(resultIntent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                handleNotification(object);
            }
            if (remoteMessage.getData().size() > 0) {
                Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
                try {
                    Map<String, String> params = remoteMessage.getData();
                    JSONObject object = new JSONObject(params);
                    handleDataMessage(object);
                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }
        }
    }

    private void handleNotification(JSONObject json) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Constant.PUSH_NOTIFICATION);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            // new SessionManager(getApplicationContext()).isNotification(true);
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) {
        Intent resultIntent = null;
        Log.e(TAG, "push json: " + json.toString());
        try {
            JSONObject data = json;
            String title = data.getString(Constant.TITLE);
            String message = data.getString(Constant.BODY);
            String orderId = data.getString(Constant.ORDER_ID);
            String organisationId = data.getString(Constant.ORGANISATION_ID);
            String key = data.getString(Constant.KEY);
            Log.e(TAG, "body: " + message);

            resultIntent = new Intent(getApplicationContext(), NotificationActivity.class);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(getApplicationContext());
            taskStackBuilder.addNextIntentWithParentStack(resultIntent);
            PendingIntent pendingNotificationIntent = PendingIntent.getActivity(getApplicationContext(), notificationIndex, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
            showNotificationMessage(getApplicationContext(), title, message, "", resultIntent, pendingNotificationIntent);


        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent, PendingIntent pendingNotificationIntent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, pendingNotificationIntent);
    }
}