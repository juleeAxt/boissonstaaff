package com.theboissonstaff.utility;


import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import com.theboissonstaff.R;
import com.theboissonstaff.activity.DashboardActivity;
import com.theboissonstaff.activity.LoginActivity;

import com.theboissonstaff.model.UserModel;
import com.theboissonstaff.server_networking.JsonArrayResponseListener;
import com.theboissonstaff.server_networking.NetworkHelper;
import com.theboissonstaff.server_networking.RequestHelper;

import org.json.JSONArray;

public class SessionManager implements JsonArrayResponseListener {
    private Context context;
    private static final String APP_PREF_NAME = "the boisson";
    private static final String APP_PREF = "boisson";
    private SharedPreferences appPref;
    private SharedPreferences passwordAppPref;
    private SharedPreferences.Editor appPrefEditor;
    private SharedPreferences.Editor passwordAppPrefEditor;

    public SessionManager(Context context) {
        this.context = context;
        appPref = context.getSharedPreferences(APP_PREF_NAME, Context.MODE_PRIVATE);
        appPrefEditor = appPref.edit();
        appPrefEditor.apply();

        passwordAppPref = context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        passwordAppPrefEditor = passwordAppPref.edit();
        passwordAppPrefEditor.apply();
    }

    public void saveUserDetail(UserModel userModel) {
        appPrefEditor.putString(Constant.ID, userModel.getId());
        appPrefEditor.putString(Constant.EMAIL, userModel.getEmail());
        appPrefEditor.putString(Constant.USERNAME, userModel.getUsername());
        appPrefEditor.putString(Constant.ORGANISATION_ID, userModel.getOrganisationId());
        appPrefEditor.putString(Constant.FIRST_NAME, userModel.getProfileModel().getFirstName());
        appPrefEditor.putString(Constant.LAST_NAME, userModel.getProfileModel().getLastName());
        appPrefEditor.putString(Constant.GENDER, userModel.getProfileModel().getGender());
        appPrefEditor.putString(Constant.AVATAR, userModel.getProfileModel().getAvatar());
        appPrefEditor.putString(Constant.AVATAR_URL, userModel.getProfileModel().getAvatarUrl());
        appPrefEditor.putString(Constant.CITY, userModel.getProfileModel().getCity());
        appPrefEditor.putInt(Constant.DISTANCE, userModel.getOrganisationModel().getDistance());
        appPrefEditor.putString(Constant.LATITUDE, userModel.getOrganisationModel().getLatitude());
        appPrefEditor.putString(Constant.LONGITUDE, userModel.getOrganisationModel().getLongitude());
        appPrefEditor.putBoolean(Constant.IS_LOGIN, true);
        appPrefEditor.commit();
    }


    public boolean isLogin() {
        return appPref.getBoolean(Constant.IS_LOGIN, false);
    }


    public String getUserId() {
        return appPref.getString(Constant.ID, "");
    }

    public String getOrganisationId() {
        return appPref.getString(Constant.ORGANISATION_ID, "");
    }

    public int getDistance() {
        return appPref.getInt(Constant.DISTANCE, 0);
    }

    public String getLatitude() {
        return appPref.getString(Constant.LATITUDE, "");
    }

    public String getLongitude() {
        return appPref.getString(Constant.LONGITUDE, "");
    }

    public String getFirstName() {
        return appPref.getString(Constant.FIRST_NAME, "");
    }

    public String getAvatarUrl() {
        return appPref.getString(Constant.AVATAR_URL, "");
    }

    public String getAccessToken() {
        return appPref.getString(Constant.ACCESS_TOKEN, "");
    }

    public String getEmail() {
        return appPref.getString(Constant.EMAIL, "");
    }

    public String getRememberPassword() {
        return passwordAppPref.getString(Constant.PASSWORD, "");
    }
    public String getRememberUserName() {
        return passwordAppPref.getString(Constant.USERNAME, "");
    }

    public void setRememberPassword(String password) {
        passwordAppPrefEditor.putString(Constant.PASSWORD, password);
        passwordAppPrefEditor.commit();
    }

    public void setRememberUserName(String userName) {
        passwordAppPrefEditor.putString(Constant.USERNAME, userName);
        passwordAppPrefEditor.commit();
    }

    public void setAccessToken(String accessToken) {
        appPrefEditor.putString(Constant.ACCESS_TOKEN, accessToken);
        appPrefEditor.commit();
    }

    public void logout() {
        logoutAlert();
    }

    private void logoutAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogCustom);
        builder.setMessage(R.string.logout_alert_msg);
        builder.setCancelable(true);
        builder.setPositiveButton(
                context.getText(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        logoutDeleteApi();
                    }
                });

        builder.setNegativeButton(
                context.getText(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert1 = builder.create();
        alert1.show();
    }

    private void logoutDeleteApi() {
        if (ConnectionUtil.isInternetOn(context)) {
            RequestHelper.deletelogoutRequest(NetworkHelper.REQ_CODE_lOGOUT_DELETE, context, Urls.LOGOUT_URL, this);
        } else {
            BaseUtility.toastMsg(context,context.getString(R.string.no_internet_connection));
        }
    }


    @Override
    public void onSuccess(int requestCode, JSONArray json) {
        switch (requestCode) {
            case NetworkHelper.REQ_CODE_lOGOUT_DELETE:
                appPrefEditor.clear();
                appPrefEditor.commit();
                BaseUtility.sentToScreenIntentWithFlag(context, LoginActivity.class);
                break;
        }
        }

    @Override
    public void onError() {
    }
}