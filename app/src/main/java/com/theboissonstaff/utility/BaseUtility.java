package com.theboissonstaff.utility;

import android.annotation.SuppressLint;
import android.app.ActivityManager;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;

import android.util.Log;
import android.view.Gravity;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import android.widget.TextView;
import android.widget.Toast;

import androidx.transition.Slide;
import androidx.transition.Transition;

import com.theboissonstaff.R;
import com.theboissonstaff.model.CartsModel;
import com.theboissonstaff.sqlite_helper.DatabaseHandler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BaseUtility {
    // this method send one activity to another activity
    public static void toastMsg(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void sendActivityIntent(Context context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
    }

    public static void sentToScreenIntentWithFlag(Context context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static String getCurrentTimeStamp() {
        long time = System.currentTimeMillis();
        Log.e("time = ", time + "");
        return String.valueOf(time);
    }

    public static void animSlide(Context context, LinearLayout fm, int anim) {
        Animation animation = AnimationUtils.loadAnimation(context.getApplicationContext(),
                anim);
        fm.setAnimation(animation);
    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                            Log.e("isAppIsInBackground = ", isInBackground + "");
                        }
                    }
                }
            }
        }

        return isInBackground;
    }

    public static void slideToTop(LinearLayout llTopSlider) {
        Transition transition = new Slide(Gravity.TOP);
        transition.setDuration(300);
        transition.addTarget(llTopSlider);
    }

    public static void slideToBottom(LinearLayout llTopSlider) {
        Transition transition = new Slide(Gravity.BOTTOM);
        transition.setDuration(300);
        transition.addTarget(llTopSlider);
    }

    public static float totalPrice(String price, String quantity) {
        float totalPrice = Float.parseFloat(price) * Integer.parseInt(quantity);
        return totalPrice;
    }

    public static void setCartsCount(TextView textView, Context mContext) {
        DatabaseHandler handler = new DatabaseHandler(mContext);
        ArrayList<CartsModel> cartsModelArrayList = handler.getData();
        if (cartsModelArrayList != null && !cartsModelArrayList.isEmpty()) {
            textView.setText(String.valueOf(cartsModelArrayList.size()));
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }

    }

    @SuppressLint("SimpleDateFormat")
    public static String convertDateFormat(String date) {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat output = new SimpleDateFormat("MMMM dd ,yyyy HH:mm a");

        Date d = null;
        try {
            d = input.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formatted = output.format(d);
        return formatted;
    }



    public static boolean distanceInMeter(Context context, double lat1, double lon1) {
        try {
            SessionManager manager = new SessionManager(context);
            Location startPoint = new Location("locationA");
            startPoint.setLatitude(lat1);
            startPoint.setLongitude(lon1);
            Location endPoint = new Location("locationA");
            endPoint.setLatitude(Double.parseDouble(new SessionManager(context).getLatitude()));
            endPoint.setLongitude(Double.parseDouble(new SessionManager(context).getLongitude()));
            double distance = startPoint.distanceTo(endPoint);
            Log.e("distance = ", distance + "");
            if (distance >= manager.getDistance()) {
                BaseUtility.toastMsg(context, context.getString(R.string.distance_error_msg));
                return false;
            }
        }catch (Exception e){
            Log.e("error = ",e.getMessage());
        }
        return true;
    }
}