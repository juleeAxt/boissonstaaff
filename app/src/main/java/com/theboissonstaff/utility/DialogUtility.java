package com.theboissonstaff.utility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;


import com.theboissonstaff.R;
import com.theboissonstaff.myInterface.AlertDialogListener;

import java.util.Objects;


public class DialogUtility {

    public static void alertDialog(Context context, AlertDialogListener alertDialogListener, String msg,String flag) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogCustom);
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setPositiveButton(
                context.getText(R.string.confirm),
                (dialog, id) -> {
                    alertDialogListener.onConfirm(flag);
                    dialog.cancel();
                });

        builder.setNegativeButton(
                context.getText(R.string.cancel),
                (dialog, id) -> {
                    dialog.cancel();
                });

        AlertDialog alert1 = builder.create();
        alert1.show();
    }
    ///Notification Dialog
    public static void notificationDialog(final Context context) {
        Button btnOrderAccept, btnOrderReject;
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogview = inflater.inflate(R.layout.dialog_notification, null);
        AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(context);
        dialogbuilder.setView(dialogview);
        final AlertDialog alertDialog = dialogbuilder.create();
        Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        btnOrderAccept = dialogview.findViewById(R.id.btn_order_accept);
        btnOrderReject = dialogview.findViewById(R.id.btn_order_reject);
        btnOrderAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String btnSelction = btnOrderAccept.getText().toString();
                selctionBtn(context, btnSelction, btnOrderAccept, btnOrderReject);
            }
        });
        btnOrderReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String btnSelction = btnOrderReject.getText().toString();
                selctionBtn(context, btnSelction, btnOrderAccept, btnOrderReject);
                //alertDialog.dismiss();
            }
        });


        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

    }

    public static void selctionBtn(Context context, String isClickAcceptBtn, Button btnOrderAccept, Button btnOrderReject) {
        if (isClickAcceptBtn.equals(context.getString(R.string.orderaccept))) {
            btnOrderAccept.setBackgroundResource(R.drawable.bg_order_accept);
            btnOrderAccept.setTextColor(context.getResources().getColor(R.color.white));
            btnOrderReject.setBackgroundResource(R.drawable.bg_order_not_reject);
            btnOrderReject.setTextColor(context.getResources().getColor(R.color.red_color));
        } else if (isClickAcceptBtn.equals(context.getString(R.string.orderreject))) {
            btnOrderReject.setBackgroundResource(R.drawable.bg_order_reject);
            btnOrderReject.setTextColor(context.getResources().getColor(R.color.white));
            btnOrderAccept.setBackgroundResource(R.drawable.bg_order_not_accept);
            btnOrderAccept.setTextColor(context.getResources().getColor(R.color.green_color));

        }
    }

}