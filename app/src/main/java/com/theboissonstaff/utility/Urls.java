package com.theboissonstaff.utility;

public class Urls {
    public static final String SHORT = "&sort=name,ASC";
    public static final String SHORT_TABLENO = "&sort=tableNumber,ASC";
    //public static final String BASE_URL = "https://api.boisson.app/api/v1/";
    public static final String BASE_URL = "http://172.104.206.212:3000/api/v1/";
    public static final String LOGIN_URL = BASE_URL + "authentication/login-app";
    public static final String VERIFY_URL = BASE_URL + "authentication/verify";
    public static final String LOGOUT_URL = BASE_URL + "authentication/logout";
    public static final String MENU_CATEGORIES = BASE_URL + "menu-categories?s=";
    public static final String MENU_ITEMS = BASE_URL + "menu-items/";
    public static final String MENU_TABLE = BASE_URL + "tables/";
    public static final String ORDER = BASE_URL + "orders";
    public static final String ORDER_BULk = BASE_URL + "order-lines/bulk";
    public static final String ORDERS_URL = BASE_URL + "orders?s=";
    public static final String ORDERS_ACCEPT_URL = BASE_URL + "orders/";
    public static final String ORDERS_DELETE_URL = BASE_URL + "order-lines/";
}
