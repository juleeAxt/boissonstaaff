package com.theboissonstaff.server_networking;

import android.content.Context;

import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.theboissonstaff.utility.BaseUtility;
import com.theboissonstaff.utility.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Response;

public class RequestHelper {
    private static final String TAG = RequestHelper.class.getSimpleName();

    public static void PostRequest(final int requestCode, final Context context, String url, JSONObject jsonObject, final ResponseListener responseListener) {
        AndroidNetworking.post(url)
                .addJSONObjectBody(jsonObject)
                .addHeaders("Content-Type", "application/json")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        responseListener.onSuccess(requestCode, response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (anError.getErrorCode() != 0) {
                            // received error from server
                            // error.getErrorCode() - the error code from server
                            // error.getErrorBody() - the error body from server
                            // error.getErrorDetail() - just an error detail
                            Log.e(TAG, "onError errorCode : " + anError.getErrorCode());
                            Log.e(TAG, "onError errorBody : " + anError.getErrorBody());
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                            // get parsed error object (If ApiError is your class)
                            if (anError.getErrorCode() == 404) {
                                String json = anError.getErrorBody();
                                try {
                                    JSONObject object = new JSONObject(json);
                                    String msg = object.optString("message");
                                    BaseUtility.toastMsg(context, msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                        }
                        responseListener.onError();
                    }
                });
    }
    public static void PatchRequest(final int requestCode, final Context context, String url, JSONObject jsonObject, final ResponseListener responseListener) {
        AndroidNetworking.patch(url)
                .addJSONObjectBody(jsonObject)
                .addHeaders("Content-Type", "application/json")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        responseListener.onSuccess(requestCode, response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (anError.getErrorCode() != 0) {
                            // received error from server
                            // error.getErrorCode() - the error code from server
                            // error.getErrorBody() - the error body from server
                            // error.getErrorDetail() - just an error detail
                            Log.e(TAG, "onError errorCode : " + anError.getErrorCode());
                            Log.e(TAG, "onError errorBody : " + anError.getErrorBody());
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                            // get parsed error object (If ApiError is your class)
                            if (anError.getErrorCode() == 404) {
                                String json = anError.getErrorBody();
                                try {
                                    JSONObject object = new JSONObject(json);
                                    String msg = object.optString("message");
                                    BaseUtility.toastMsg(context, msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                        }
                        responseListener.onError();
                    }
                });
    }

    public static void PostRequestWithoutHeader(final int requestCode, final Context context, String url, JSONObject jsonObject, final ResponseListener responseListener) {
        AndroidNetworking.post(url)
                .addJSONObjectBody(jsonObject)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        responseListener.onSuccess(requestCode, response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (anError.getErrorCode() != 0) {
                            // received error from server
                            // error.getErrorCode() - the error code from server
                            // error.getErrorBody() - the error body from server
                            // error.getErrorDetail() - just an error detail
                            Log.e(TAG, "onError errorCode : " + anError.getErrorCode());
                            Log.e(TAG, "onError errorBody : " + anError.getErrorBody());
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                            // get parsed error object (If ApiError is your class)
                            if (anError.getErrorCode() == 404) {
                                String json = anError.getErrorBody();
                                try {
                                    JSONObject object = new JSONObject(json);
                                    String msg = object.optString("message");
                                    BaseUtility.toastMsg(context, msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                        }
                        responseListener.onError();
                    }
                });
    }


    public static void getRequest(final int requestCode, final Context context, String url, final JsonArrayResponseListener responseListener) {
        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e("response = ", response + "");
                        responseListener.onSuccess(requestCode, response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (anError.getErrorCode() != 0) {
                            Log.e(TAG, "onError errorCode : " + anError.getErrorCode());
                            Log.e(TAG, "onError errorBody : " + anError.getErrorBody());
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                            if (anError.getErrorCode() == 404) {
                                String json = anError.getErrorBody();
                                try {
                                    JSONObject object = new JSONObject(json);
                                    String msg = object.optString("message");
                                    BaseUtility.toastMsg(context, msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                        }
                        responseListener.onError();
                    }
                });
    }

    public static void getRequestReturnObject(final int requestCode, final Context context, String url, final ResponseListener responseListener) {
        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response = ", response + "");
                        responseListener.onSuccess(requestCode, response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (anError.getErrorCode() != 0) {
                            Log.e(TAG, "onError errorCode : " + anError.getErrorCode());
                            Log.e(TAG, "onError errorBody : " + anError.getErrorBody());
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                            if (anError.getErrorCode() == 404) {
                                String json = anError.getErrorBody();
                                try {
                                    JSONObject object = new JSONObject(json);
                                    String msg = object.optString("message");
                                    BaseUtility.toastMsg(context, msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                        }
                        responseListener.onError();
                    }
                });
    }

    public static void postRequest(final int requestCode, final Context context, String url, JSONObject jsonObject, final JsonArrayResponseListener responseListener) {
        AndroidNetworking.post(url)
                .addJSONObjectBody(jsonObject)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e("response = ", response + "");
                        responseListener.onSuccess(requestCode, response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (anError.getErrorCode() != 0) {
                            Log.e(TAG, "onError errorCode : " + anError.getErrorCode());
                            Log.e(TAG, "onError errorBody : " + anError.getErrorBody());
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                            if (anError.getErrorCode() == 404) {
                                String json = anError.getErrorBody();
                                try {
                                    JSONObject object = new JSONObject(json);
                                    String msg = object.optString("message");
                                    BaseUtility.toastMsg(context, msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                        }
                        responseListener.onError();
                    }
                });
    }


    public static void verifyHeaderRequest(Context mContext, final int requestCode, String url, ResponseListener responseListener) {
        AndroidNetworking.get(url)
                .addHeaders("Authorization", new SessionManager(mContext).getAccessToken())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        responseListener.onSuccess(requestCode, response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (anError.getErrorCode() != 0) {
                            // received error from server
                            // error.getErrorCode() - the error code from server
                            // error.getErrorBody() - the error body from server
                            // error.getErrorDetail() - just an error detail
                            Log.e(TAG, "onError errorCode : " + anError.getErrorCode());
                            Log.e(TAG, "onError errorBody : " + anError.getErrorBody());
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                            // get parsed error object (If ApiError is your class)

                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                        }
                        responseListener.onError();
                    }
                });
    }

    public static void deleteRequest(final int requestCode, final Context context, String url, JsonArrayResponseListener responseListener) {
        AndroidNetworking.delete(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsOkHttpResponse(new OkHttpResponseListener() {
                    @Override
                    public void onResponse(Response response) {
                        if (response.code() == 200) {
                            responseListener.onSuccess(requestCode, null);
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        if (anError.getErrorCode() != 0) {
                            // received error from server
                            // error.getErrorCode() - the error code from server
                            // error.getErrorBody() - the error body from server
                            // error.getErrorDetail() - just an error detail
                            Log.e(TAG, "onError errorCode : " + anError.getErrorCode());
                            Log.e(TAG, "onError errorBody : " + anError.getErrorBody());
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                            // get parsed error object (If ApiError is your class)
                            if (anError.getErrorCode() == 404) {
                                String json = anError.getErrorBody();
                                try {
                                    JSONObject object = new JSONObject(json);
                                    String msg = object.optString("message");
                                    BaseUtility.toastMsg(context, msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                        }
                        responseListener.onError();
                    }
                });


    }
    public static void deletelogoutRequest(final int requestCode, final Context context, String url, JsonArrayResponseListener responseListener) {
        AndroidNetworking.delete(url)
                .addHeaders("Authorization", new SessionManager(context).getAccessToken())
                .setPriority(Priority.HIGH)
                .build()
                .getAsOkHttpResponse(new OkHttpResponseListener() {
                    @Override
                    public void onResponse(Response response) {
                        if (response.code() == 200) {
                            responseListener.onSuccess(requestCode, null);
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        if (anError.getErrorCode() != 0) {
                            // received error from server
                            // error.getErrorCode() - the error code from server
                            // error.getErrorBody() - the error body from server
                            // error.getErrorDetail() - just an error detail
                            Log.e(TAG, "onError errorCode : " + anError.getErrorCode());
                            Log.e(TAG, "onError errorBody : " + anError.getErrorBody());
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                            // get parsed error object (If ApiError is your class)
                            if (anError.getErrorCode() == 404) {
                                String json = anError.getErrorBody();
                                try {
                                    JSONObject object = new JSONObject(json);
                                    String msg = object.optString("message");
                                    BaseUtility.toastMsg(context, msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.e(TAG, "onError errorDetail : " + anError.getErrorDetail());
                        }
                        responseListener.onError();
                    }
                });


    }

}