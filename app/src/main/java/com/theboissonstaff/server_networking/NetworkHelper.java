package com.theboissonstaff.server_networking;

import android.content.Context;
import android.util.Log;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;
import com.theboissonstaff.activity.OrderDetailActivity;
import com.theboissonstaff.fragment.CartFragment;
import com.theboissonstaff.model.CartsModel;
import com.theboissonstaff.utility.Constant;
import com.theboissonstaff.utility.ProgressDialogUtil;
import com.theboissonstaff.utility.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class NetworkHelper {


    private static final String TAG = NetworkHelper.class.getSimpleName();
    private Context mContext;

    public static final int REQ_CODE_LOGIN = 1;
    public static final int REQ_CODE_VERIFY = 2;
    public static final int REQ_CODE_MENU = 3;
    public static final int REQ_CODE_MENU_CATEGORY = 4;
    public static final int REQ_CODE_FLAVOURS = 5;
    public static final int REQ_CODE_TABLE = 6;
    public static final int REQ_CODE_PLACE_ORDER = 7;
    public static final int REQ_CODE_ORDER_ON_GOING = 8;
    public static final int REQ_CODE_ORDER_COMPLETE = 9;
    public static final int REQ_CODE_ORDER_DELETE = 10;
    public static final int REQ_CODE_lOGOUT_DELETE = 11;
    public static final int REQ_CODE_ORDER_NOTIFICATION =12 ;
    public static final int REQ_CODE_ORDER_ACCEPT = 13;
    private String userId;

    public NetworkHelper(Context context) {
        mContext = context;
        userId = new SessionManager(context).getUserId();
    }

    public JSONObject loginJson(String userName, String password) {
        JSONObject object = new JSONObject();
        try {
            object.putOpt(Constant.USERNAME, userName);
            object.putOpt(Constant.PASSWORD, password);
            object.putOpt(Constant.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken());
            object.putOpt(Constant.ROLE, "waiter");
            object.putOpt(Constant.FCM_PLATFORM, Constant.FCM_PLATFORM_VALUE);
            Log.e(TAG, object + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    public String menuCategory() {
        JSONObject object = new JSONObject();

        try {
            object.putOpt(Constant.ORGANISATION_ID, new SessionManager(mContext).getOrganisationId());
            object.putOpt(Constant.PARENT, JSONObject.NULL);
            object.putOpt(Constant.IS_ACTIVE, true);
            Log.e(TAG, object + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }

    public String menuItemJson(String parentId) {
        JSONObject object = new JSONObject();
        try {
            object.putOpt(Constant.ORGANISATION_ID, new SessionManager(mContext).getOrganisationId());
            object.putOpt(Constant.PARENT_ID, parentId);
            object.putOpt(Constant.MENU_ITEMS_IS_ACTIVE, true);
            Log.e(TAG, object + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }

    public String tableJson() {
        JSONObject object = new JSONObject();
        try {
            object.putOpt(Constant.ORGANISATION_ID, new SessionManager(mContext).getOrganisationId());
            Log.e(TAG, object + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }

    public JSONObject placeOrder(ArrayList<CartsModel> list, String tableId) {
        String orgId = "";
        JSONObject object = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();
            for (CartsModel model : list) {
                orgId = model.getOrganisationId();
                JSONObject jsonObject = new JSONObject();
                jsonObject.putOpt(Constant.QUANTITY, model.getQuantity());
                jsonObject.putOpt(Constant.MENU_ITEM_ID, model.getItemId());
                jsonObject.putOpt(Constant.FLAVOUR_ID, model.getFlavourId());
                jsonObject.putOpt(Constant.PRICE, model.getPrice());
                jsonArray.put(jsonObject);
            }
            object.putOpt(Constant.TABLE_ID, tableId);
            object.putOpt(Constant.WAITER_ID, new SessionManager(mContext).getUserId());
            object.putOpt(Constant.ORGANISATION_ID, orgId);
            object.putOpt(Constant.ORDER_LINES, jsonArray);
            Log.e(TAG, object + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    public JSONObject placeBulkOrder(ArrayList<CartsModel> list) {
        JSONObject object = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();
            for (CartsModel model : list) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.putOpt(Constant.QUANTITY, model.getQuantity());
                jsonObject.putOpt(Constant.MENU_ITEM_ID, model.getItemId());
                jsonObject.putOpt(Constant.FLAVOUR_ID, model.getFlavourId());
                jsonObject.putOpt(Constant.PRICE, model.getPrice());
                jsonObject.putOpt(Constant.ORDER_ID, OrderDetailActivity.addOrderId);
                jsonArray.put(jsonObject);
            }

            object.putOpt(Constant.BULK, jsonArray);
            Log.e(TAG, object + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    public String orderOnGoingJson(String status) {
        JSONObject object = new JSONObject();
        try {
            object.putOpt(Constant.WAITER_ID, userId);
            object.putOpt(Constant.STATUS, status);
            Log.e(TAG, object + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }

    public String orderDetailsJson(String orgId, String id) {
        JSONObject object = new JSONObject();
        try {
            object.putOpt(Constant.WAITER_ID, userId);
            object.putOpt(Constant.ORGANISATION_ID, orgId);
            object.putOpt(Constant.ID, id);
            Log.e(TAG, object + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }

    public String getNewOrder(String orderId) {
        JSONObject object = new JSONObject();
        try {
            object.putOpt(Constant.ID,orderId);
            Log.e(TAG, object + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }
    public JSONObject getOrderAccept() {
        JSONObject object = new JSONObject();
        try {
            object.putOpt(Constant.WAITER_ID,new SessionManager(mContext).getUserId());
            Log.e(TAG, object + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }
}