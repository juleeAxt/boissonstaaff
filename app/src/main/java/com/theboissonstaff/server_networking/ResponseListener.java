package com.theboissonstaff.server_networking;

import org.json.JSONObject;


public interface ResponseListener {
    /**
     * Method will be called when request made and returned response successfully.
     */
    void onSuccess(int requestCode, JSONObject json);

    /**
     * Method will be called when error occurred during  request.
     */
    void onError();
}
