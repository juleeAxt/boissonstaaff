package com.theboissonstaff.server_networking;

import org.json.JSONArray;


public interface JsonArrayResponseListener {
    /**
     * Method will be called when request made and returned response successfully.
     */
    void onSuccess(int requestCode, JSONArray json);

    /**
     * Method will be called when error occurred during  request.
     */
    void onError();
}
